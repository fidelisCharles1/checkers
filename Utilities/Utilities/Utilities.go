package Utilities

import(
	humanize "github.com/dustin/go-humanize"     
    Mathrand "math/rand"
    Conf "checkers/Utilities/Conf"
    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/aws/credentials"
    "github.com/aws/aws-sdk-go/service/s3/s3manager"
    "path/filepath"    
	"net/http"
    "crypto/aes"
    "crypto/cipher"
    "crypto/rand"
    "encoding/base64"
    "encoding/json"
    "crypto/md5"
    "encoding/hex"
    "io"
    "io/ioutil"
    "bufio"
    "time"
    "bytes"
    "strconv"
    "math"
    "log"
    "fmt"
    "os"
    "strings"
    "errors"
    "image/jpeg"
    "image/png"
    //for parsing html
    "golang.org/x/net/html"
)

type CachePayload struct{
    Key string `json:"key,omitempty"`
    Data []byte  `json:"data,omitempty"`
}

type APIResponse struct{
	Code int `json:"code"`
	Message string `json:"message"`
	Body interface{}   `json:"payload"`
}

func (resp *APIResponse)ToString()string{
	bytes,_ := json.Marshal(resp)
	return string(bytes)
}

//this function is aimed to process internal API call responses to bring out byte arrays
//these byte arrays can then be turned into what the needs are at the time of the call
func GetAPIResponseFromHTTPResponse(res *http.Response)([]byte,error){
    var APIResponse APIResponse
    var bts []byte
    var err error

    bts,err = ioutil.ReadAll(res.Body)
    if(err!=nil){
        goto ret
    }

    err = json.Unmarshal(bts,&APIResponse)
    if(err != nil){
        goto ret
    }

    if(APIResponse.Code != 200){
        err = errors.New(APIResponse.Message)
        goto ret
    }

    bts,err = json.Marshal(APIResponse.Body)
    if(err != nil){
        goto ret
    }

    ret:
    return bts,err
}

func EnableCors(w *http.ResponseWriter,r *http.Request) {
    var host string = "*"

    if header ,ok := r.Header["Origin"]; ok{
        host = header[0]
    }

    (*w).Header().Set("Access-Control-Allow-Headers", "Authorization,Cookie")
    (*w).Header().Set("Access-Control-Allow-Origin", host)
    (*w).Header().Set("Access-Control-Allow-Credentials", "true")
    (*w).Header().Set("Access-Control-Allow-Methods","GET,POST,OPTIONS")
    (*w).Header().Set("Content-Type", "application/json")
}

func SecurityErrorResponse(w *http.ResponseWriter,err string){
    switch(err){
        case "OPTIONS":
            fmt.Fprintf(*w,err)
        default:
            http.Error(*w, "{\"code\":401,\"description\":\""+Conf.FailedAuthoriation+"\"}", 401)
    }
}

func DecryptAccesToken(accessToken string)string{
    accessToken = DecryptEntry(accessToken)
    accessToken = strings.Replace(accessToken,Conf.AdditionalSalt,"",-1)
    return DecryptEntry(accessToken)
}


var mobileOperatorPrefixes = map[string]string{"74":"Vodacom","75":"Vodacom","76":"Vodacom","67":"Tigo","65":"Tigo","71":"Tigo","68":"Airtel",
											 "69":"Airtel","78":"Airtel","62":"Halotel","73":"TTCL","77":"Zantel","66":"Smile"}

func FormatMobileNo(mobile string)string{
    const prefix = "255"
    if(len(mobile)==10 && string(mobile[0])=="0"){
        mobile = prefix+string(mobile[1:])
    }else if(len(mobile)==9 && string(mobile[0])!="0"){
        mobile = prefix+mobile        
    }else if(len(mobile)==13 && string(mobile[:3])=="+255"){
        mobile = prefix+string(mobile[1:])
    }

    return mobile
}

func IncrementDates(inputDate string,incrementValue int)[]string{
    var returns []string
    parsedTime,_ := time.Parse(Conf.MysqlDateFormat,inputDate)
    for x := 0 ; x <= incrementValue ; x++{
        var t = -1 * x
        tmp := parsedTime.AddDate(0,0,t)
        returns = append(returns,tmp.Format(Conf.MysqlDateFormat))
    }

    return returns
}

//function to ensure the date and month are always in accordance with golang's
//i.e the second of february will be set as YEAR-02-02 instead of YEAR-2-2 
func formatDateString(date string)string{
    dateSplit := strings.Split(date,"-")
    if( len(dateSplit[1]) == 1 ){
        dateSplit[1] = "0"+dateSplit[1]
    }

    if( len(dateSplit[2]) == 1 ){
        dateSplit[2] = "0"+dateSplit[2]
    }

    return strings.Join(dateSplit,"-")
}

func DifferenceBetweenDates(date1 string,date2 string)float64{
    date1 = formatDateString(date1)
    date2 = formatDateString(date2)
    parsedTime1,_ := time.Parse(Conf.MysqlDateFormat,date1)
    parsedTime2,_ := time.Parse(Conf.MysqlDateFormat,date2)
    //add 1 date to include today's date
    parsedTime2 = parsedTime2.AddDate(0,0,1)
    var td float64
    td = parsedTime1.Sub(parsedTime2).Hours()   

    return td
}

func DetermineYearsSinceGivenDate(date string)float64{
    parsedTime1,_ := time.Parse(Conf.MysqlDateFormat,date)
    parsedTime2 := time.Now()
    var td float64
    td = parsedTime1.Sub(parsedTime2).Hours()   
    if(td<0){
        td = td * -1.0
    }
    //td is in hours convert that to years
    td = td / ( 364 * 24 ) 

    return td
}

func FormatDate(inputDate string)string{
    parsedTime,_ := time.Parse(Conf.MysqlDateFormat,inputDate)
    return parsedTime.Format(Conf.MysqlDateFormat)   
}

func GetDate()string{
    return time.Now().Format(Conf.MysqlDateFormat)
}

func GetTimeStamp()int64{
    return time.Now().Unix()
}

func DetermineOperatorFromMobile(mobile string)(string,bool){

	var returnMessage string
	var returnFlag bool
	if(len(mobile)>13 || len(mobile)<9){
		return "",false
	}

	switch(string(mobile[0])){
	case "0":
		if(len(mobile)!=10){
			returnFlag = false
		}else{
			returnMessage = mobileOperatorPrefixes[string(mobile[1])+string(mobile[2])]
			returnFlag = true
		}
	case "2":
		if( string(mobile[1])=="5" && string(mobile[2])=="5" && len(mobile)==12){
			returnMessage = mobileOperatorPrefixes[string(mobile[3])+string(mobile[4])]
			returnFlag = true
		}else{
			returnFlag = false
		}
	case "+":
		if( string(mobile[1])=="2" && string(mobile[2])=="5" && string(mobile[3])=="5" && len(mobile)==13){
			returnMessage = mobileOperatorPrefixes[string(mobile[4])+string(mobile[5])]
			returnFlag = true
		}else{
			returnFlag = false
		}
	default :
			returnFlag = false
	}

	return returnMessage,returnFlag
}

func SetCookie(cookieName string,cookieValue string,rw http.ResponseWriter){
	cookie := http.Cookie{
		Name:     cookieName,
		Value:    cookieValue,
		MaxAge:   Conf.CookieLifeTime,
		HttpOnly: Conf.HTTPOnlyCookies,
		Secure : Conf.SecureCookies,
        Path : "/",
	}
	http.SetCookie(rw, &cookie)
}

func UnsetCookie(cookieName string,rw http.ResponseWriter){
	cookie := http.Cookie{
		Name:     cookieName,
		Value:    "",
		MaxAge:   -10,
		HttpOnly: true,
		Secure : false,
	}
	http.SetCookie(rw, &cookie)	
}

func GetCookie(cookieName string,r *http.Request)string{
	 cookie, err := r.Cookie(cookieName)
	 if(err != nil){
	 	if(err.Error()=="http: named cookie not present"){
	 		return ""
	 	}else{
		 	log.Println(err)
		 	return ""
	 	}
	 }

	 return cookie.Value
}

func createHash(key string) string {
    hasher := md5.New()
    hasher.Write([]byte(key))
    return hex.EncodeToString(hasher.Sum(nil))
}


// encrypt string to base64 crypto using AES
func encrypt(key []byte, text string) string {
    // key := []byte(keyText)
    plaintext := []byte(text)

    block, err := aes.NewCipher(key)
    if err != nil {
        panic(err)
    }

    // The IV needs to be unique, but not secure. Therefore it's common to
    // include it at the beginning of the ciphertext.
    ciphertext := make([]byte, aes.BlockSize+len(plaintext))
    iv := ciphertext[:aes.BlockSize]
    if _, err := io.ReadFull(rand.Reader, iv); err != nil {
        panic(err)
    }

    stream := cipher.NewCFBEncrypter(block, iv)
    stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

    // convert to base64
    return base64.URLEncoding.EncodeToString(ciphertext)
}

// decrypt from base64 to decrypted string
func decrypt(key []byte, cryptoText string) string {
    ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

    block, err := aes.NewCipher(key)
    if err != nil {
        panic(err)
    }

    // The IV needs to be unique, but not secure. Therefore it's common to
    // include it at the beginning of the ciphertext.
    if len(ciphertext) < aes.BlockSize {
        panic("ciphertext too short")
    }
    iv := ciphertext[:aes.BlockSize]
    ciphertext = ciphertext[aes.BlockSize:]

    stream := cipher.NewCFBDecrypter(block, iv)

    // XORKeyStream can work in-place if the two arguments are the same.
    stream.XORKeyStream(ciphertext, ciphertext)

    return string(ciphertext)
}
/**/


func EncryptEntry(entry string)string{
	return encrypt([]byte(createHash(Conf.CIPHERPASSPHRASE)),entry)
}

func DecryptEntry(entry string)string{
	return decrypt([]byte(createHash(Conf.CIPHERPASSPHRASE)),entry)
}

func GetRandomKey()int{
    Mathrand.Seed(time.Now().UnixNano())
    no := Mathrand.Intn(6000)+Mathrand.Intn(2500)	
    return no
}

func FormatStringNumber(number string)string{
	no,_ := strconv.ParseFloat(number,32);
	return FormatNumber( float32(no) )
}

func FormatNumber(number float32)string{
	no := float64(number)
	no = math.Round(no*100)/100
	return humanize.Commaf(no)
}

func GetFileTypeFromName(fileName string)string{
    splits := strings.Split(fileName,".")
    switch( strings.ToLower( splits[ len( splits ) - 1 ] ) ){
        case "jpeg":
            return "image"
        case "jpg":
          return "image"
        case "png":
            return "image"
        case "gif":
            return "image"
        case "mp3":
            return "audio"
        case "mp4":
            return "video"
        case "webm":
            return "video"
        case "ogg":
            return "video"
        default:
            return "file"
    }

    return ""
}

func GetBase64FromFile(fileName string)string{
	f, _ := os.Open(Conf.TempPath+fileName);

	/***/
	// create a new buffer base on file size
    fInfo, _ := f.Stat()
    var size int64 = fInfo.Size()
    buf := make([]byte, size)

    // read file content into buffer
    fReader := bufio.NewReader(f)
    fReader.Read(buf)
  	/***/


	mimeType,_ := GetFileContentType(f)
	
	preBase64 :="data:"+mimeType+";base64,"; 

	preBase64 += base64.URLEncoding.EncodeToString(buf)

    f.Close();
    // Encode as base64.

	err := os.Remove(Conf.TempPath+fileName)
	if(err!=nil){
		log.Println(err)
	}

    return preBase64
}

func GetFileContentType(out *os.File) (string, error) {

    // Only the first 512 bytes are used to sniff the content type.
    buffer := make([]byte, 512)

    _, err := out.Read(buffer)
    if err != nil {
        return "", err
    }

    // Use the net/http package's handy DectectContentType function. Always returns a valid 
    // content-type by returning "application/octet-stream" if no others seemed to match.
    contentType := http.DetectContentType(buffer)

    return contentType, nil
}

func DecodeAndSaveFileToS3(baseString string)(string,error){
    var extension string
    //generating filename based on timestamp and random numbers to prevent collisions
    fileName := strconv.Itoa(GetRandomKey())+"-"+time.Now().Local().Format("20060102150405")

	//decode file
    spl := strings.Split(baseString,";")
    baseString = spl[1]
    extension = spl[0]
    spl = strings.Split(extension,"/");
    extension = spl[1];
    if(extension==""){
    	return fileName,errors.New("File extension could not be determined")
    }
    fileName += "."+extension
    spl = strings.Split(baseString,",")
    baseString = spl[1]

	dec, err := base64.StdEncoding.DecodeString(baseString)
    if err != nil {
        panic(err)
    }

    f, err := os.Create(Conf.TempPath+fileName)
    if err != nil {
        panic(err)
    }

    if _, err := f.Write(dec); err != nil {
        panic(err)
    }
    if err := f.Sync(); err != nil {
        panic(err)
    }

	//send to s3
	_,err = SaveFileToS3(Conf.TempPath+fileName)
	if(err!=nil){
		return fileName,err
	}

	//delete file
	f.Close()
	err = os.Remove(Conf.TempPath+fileName)
	if(err!=nil){
		return fileName,err
	}

	return fileName,nil
}

func pngToJPG(imageBytes []byte)(bool,[]byte,error){
    var success bool
    var buf *bytes.Buffer
    var err error
    var opt jpeg.Options

    img, err := png.Decode(bytes.NewReader(imageBytes))
    if err != nil {
        goto ret
    }

    buf = new(bytes.Buffer)
    
    opt.Quality = 80
    if err := jpeg.Encode(buf, img, &opt); err != nil {
        goto ret
    }

    success = true
    ret:
    return success,buf.Bytes(),err
}

func DecodeFile(baseString string)(string,error){
    var extension string
    var err error
    var f *os.File
    var dec []byte
    var success bool

    fileName := strconv.Itoa(GetRandomKey())+"-"+time.Now().Local().Format("20060102150405")+"-"+strconv.Itoa(len(baseString))
    //decode file
    spl := strings.Split(baseString,";")
    baseString = spl[1]
    extension = spl[0]
    spl = strings.Split(extension,"/");
    extension = spl[1];
    if(extension==""){
        err = errors.New("File extension could not be determined")
        goto ret
    }

    fileName += ".jpg"
    spl = strings.Split(baseString,",")
    baseString = spl[1]

    dec, err = base64.StdEncoding.DecodeString(baseString)
    if err != nil {
        goto ret
    }

    if(extension=="png"){
        success,dec,err = pngToJPG(dec)
        switch(success){
            case false:{
                goto ret
            }
        }
    }
   
    f, err = os.Create(Conf.TempPath+fileName)
    defer f.Close()
    if err != nil {
        goto ret
    }

    if _, err := f.Write(dec); err != nil {
        goto ret
    }
    if err := f.Sync(); err != nil {
        goto ret
    }


    ret:
    return fileName,err
}

func SaveFileToS3(filename string)(bool,error){
    myBucket := Conf.S3Bucket
    conf := aws.Config{
                        Region: aws.String(Conf.BucketRegion),
                        Credentials: credentials.NewSharedCredentials("", "noaubongo"),
                    }

    var myString string

    // The session the S3 Uploader will use
    sess := session.Must(session.NewSession(&conf))

    // Create an uploader with the session and default options
    uploader := s3manager.NewUploader(sess)

    //large image    
    myString = "resources/"+filepath.Base(filename)
    f, err  := os.Open(filename)
    if err != nil {
        return false,err
    }

    // Upload the file to S3.
    _, err = uploader.Upload(&s3manager.UploadInput{
        Bucket: aws.String(myBucket),
        Key:    aws.String(myString),
        ACL: aws.String("public-read"),
        Body:   f,
    })
    if err != nil {
        return false,err
    }
    defer f.Close()

    if err != nil {
        return false,err
    }

    return true,nil
}

func GetFileFromS3(filename string)(bool,error){
    var succesful bool
    var err error
    var resp *http.Response
    var out *os.File


    _, err = os.Stat(Conf.TempPath+filename)
    if os.IsNotExist(err) == false{
        succesful = true
        goto exitCall
    }

    // Get the data
    resp, err = http.Get(Conf.AttachmentsBasePath+filename)
    if err != nil {
        goto exitCall
    }
    defer resp.Body.Close()

    // Create the file
    out, err = os.Create(Conf.TempPath+filename)
    if err != nil {
        goto exitCall
    }
    defer out.Close()

    // Write the body to file
    _, err = io.Copy(out, resp.Body)
    if err != nil {
        goto exitCall
    }

    succesful = true

    exitCall:
    return succesful,err
}


func networkResponseErrorHandling(response *http.Response)error{
    var err error
    var bts []byte

    bts,err = ioutil.ReadAll(response.Body)
    if(err!=nil){
        goto ret
    }

    err = errors.New(string(bts))
    response.Body.Close()

    ret:
    return err
}

func Network(url string,method string,payload []byte,requestHeaders map[string]string)(*http.Response,error){
    var res *http.Response
    var req *http.Request
    var err error

    client := &http.Client{}
    req, err = http.NewRequest(method, url, bytes.NewBuffer(payload))
    if(err!=nil){
        goto ret
    }

    req.Header.Add("Content-Type", "application/json")
    req.Header.Add("Accept", "application/json")
    for key,val := range requestHeaders{
        req.Header.Add(key, val)
    }

    client.Timeout = time.Second * Conf.RequestTimeouts
    res,err = client.Do(req)
    if(err!=nil){
        goto ret
    }

    if(res.StatusCode!=200){
        err = networkResponseErrorHandling(res)
    }

    ret:
    return res,err
}

func UpdateCache(key string,value interface{})(APIResponse,error){
    const url = Conf.CacheBaseURL+"store-cache"
    var payload CachePayload
    var res *http.Response
    var err error
    var payloadBytes []byte

    payloadBytes,err = json.Marshal(value)

    payload.Key = key
    payload.Data = payloadBytes
    payloadBytes,err = json.Marshal(payload)
    if(err!=nil){
        goto ret
    }

    res,err = Network( url , "POST" , payloadBytes ,map[string]string{"Authorization":Conf.InternalAuthentication})
    if(err==nil){
        res.Body.Close()
    }

    ret:
    return cacheOutPut(res,err)
}

func DeleteCache(key string)(APIResponse,error){
    const url = Conf.CacheBaseURL+"delete-cache"
    var payload CachePayload
    var res *http.Response
    var err error
    var payloadBytes []byte

    payload.Key = key
    payloadBytes,err = json.Marshal(payload)
    if(err != nil){
       goto ret
    }

    res,err = Network( url , "POST" , payloadBytes ,map[string]string{"Authorization":Conf.InternalAuthentication} )
    if(err==nil){
        res.Body.Close()
    }

    ret:
    return cacheOutPut(res,err)
}

func GetCache(key string)([]byte,error){
    var url = Conf.CacheBaseURL+"get-cache?key="+key
    var payload CachePayload
    var res *http.Response
    var apiResponse APIResponse
    var bts []byte
    var err error

    res,err = Network( url , "GET" , nil ,map[string]string{"Authorization":Conf.InternalAuthentication} )
    if(err != nil){
        goto ret
    }
    defer res.Body.Close()

    apiResponse,err = cacheOutPut(res,err)
    if(err!=nil){
        goto ret
    }

    if(apiResponse.Code!=200){
        err = errors.New(apiResponse.Message)
        goto ret
    }

    bts,err = json.Marshal(apiResponse.Body)
    if(err!=nil){
        goto ret
    }

    err = json.Unmarshal(bts,&payload)
    if(err!=nil){
        goto ret
    }

    ret:
    return payload.Data,err
}

func cacheOutPut(res *http.Response,err error)(APIResponse,error){
    var response APIResponse
    if(res == nil){
        response.Code = 415
        response.Message = "Cache service could not be reached"
        goto ret
    }
    defer res.Body.Close()


    if( res != nil ){
        payloadBytes, err := ioutil.ReadAll(res.Body)
        if(err!=nil){
            goto ret
        }

        err = json.Unmarshal(payloadBytes,&response)
        if(err!=nil){
            goto ret
        }
    }

    ret:
    return response,err
}

/*
    This function is meant to parse HTML and extract images then convert them into path based rther than base64 based src
    meaning we extract their base 64 data and assign them a path which makes it easier to transfer to the front end and at the same time
    makes the pages lighter and save bandwidth
*/
func ParseFunc(n *html.Node,mp map[string]string){
    var ctr int
    if n.Type == html.ElementNode && n.Data == "img" {

        for _, a := range n.Attr {
            if a.Key == "src" {
                if(strings.Contains(a.Val,Conf.AttachmentsBasePath)==false){
                    mp[a.Val],_ = DecodeFile(a.Val)
                    mp[a.Val] = mp[a.Val]
                    ctr++
                }
                break                    
            }
        }
    }
    for c := n.FirstChild; c != nil; c = c.NextSibling {
        ParseFunc(c,mp)
    }
}

/*
    This is used to determine if there is an image in the body to transform it from base64 to path based
    this improves load times
*/
func ParseInput(input string)string{
    mp := make(map[string]string)
    doc, err := html.Parse(strings.NewReader(input))
    if err != nil {
        log.Fatal(err)
    }
    ParseFunc(doc,mp)
    var tmp string
    for key := range mp{
        tmp = mp[key]
        go SaveFileToS3(Conf.TempPath+tmp)
        input = strings.Replace(input,key,Conf.AttachmentsBasePath+tmp,-1)
     }
    return input
}

