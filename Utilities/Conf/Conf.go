package Conf

const Port = "8080" 
const SecurePort = "8081" 
const CachePort = "9008"

const rootUrl = "http://localhost"
const Live = true
const LocalAddress = rootUrl+":"+Port+"/"

//Security
const CIPHERPASSPHRASE = "N0@U3ONg0@)@)"
const AdditionalSalt = "-|-"+CIPHERPASSPHRASE
const FullChainFilePath = "/etc/letsencrypt/live/nbcgames.co.tz/fullchain.pem"
const PrivateKeyFilePath = "/etc/letsencrypt/live/nbcgames.co.tz/privkey.pem"
const FailedAuthoriation = "Authorization Failure"
const InternalAuthentication = "Basic "+CIPHERPASSPHRASE
const CacheBaseURL = rootUrl+":"+CachePort+"/"
var AuthorizationMap = map[string]string{"Authorization":InternalAuthentication}


//Cookies
const HTTPOnlyCookies = false
const SecureCookies = false
const CookieLifeTime = 0

const RequestTimeouts = 60
const AttachmentsBasePath = "https://s3-eu-west-1.amazonaws.com/noa-ubongo.co.tz/dynamic-assets/"
const LocalAttachmentsBasePath = "https://seebait.com/assets/temporary_files/"

const DbName = "nbc_checkers" //"checkers" 
const DbUserName = "root" //"root" 
const DbPassword = "Kwanza@)@)1"  
const DBPath = "127.0.0.1:3306"         
const ConnectionString = DbUserName+":"+DbPassword+"@tcp("+DBPath+")/"+DbName+"?charset=utf8&parseTime=True"

const BucketRegion = "eu-west-1"
const S3Bucket = "noa-ubongo"
const MysqlDateFormat = "2006-01-02"
const TempPath = "temp/"
const MaxPostSize = 10000000

const MaxIdleConnections = 5
const MaxOpenConnections = 20

//cache 
const CacheShards = 1024
const CacheLifetime = 3
const MaxEntriesInCacheWindow = 1000 * 100 * 10
const MaxEntrySize = 2048 
const VerboseCache = false
const HardMaxCacheSize = 1024 * 4