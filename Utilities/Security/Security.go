package Security

import (
		"net/http"
		"errors"
		"strings"
		"strconv"
		"time"
		"encoding/base64"
		Conf "checkers/Utilities/Conf"
		Utilities "checkers/Utilities/Utilities"
		"log"
	)

const cookieName = "access-token"

func SecurityCheck(r *http.Request)(bool,error){
	var err error
	var passed bool
	var headerData string
	var bts []byte

    if(r.Method=="OPTIONS"){
    	err = errors.New("OPTIONS")
    	goto ret
    }

	if header ,ok := r.Header["Authorization"]; ok{
		var headerSeparated []string
		headerData = strings.Replace(header[0],"Basic ","",-1)

		switch(headerData){
			//this is used for internal calls and tests
			case Conf.CIPHERPASSPHRASE:{
				passed = true
				break
			}
			default:{

				headerSeparated = strings.Split(headerData,".")
				if(len(headerSeparated)<2){
					log.Println(r.Header["Host"])
					log.Println("Step 1")
					log.Println(r.URL.String())
					log.Println("**** END ***")
					err = errors.New("Authentication Failed")
					goto ret
				}

				bts, err =  base64.StdEncoding.DecodeString(headerSeparated[0])
				if(err != nil){
					goto ret
				}
				headerSeparated[0] = string(bts)

				headerSeparated[1] = strings.Split(headerSeparated[1],"/")[0]

				if( headerSeparated[1] != Utilities.GetCookie(cookieName,r) ){
					log.Println(r.Header["Host"])
					log.Println("Step 2")
					log.Println(r.URL.String())
					log.Println("**** END ***")
					err = errors.New("Authentication Failed")
					goto ret
				}

				headerData = Utilities.DecryptAccesToken(headerSeparated[1])

				headerSeparated = strings.Split(headerSeparated[0],":")
				if(len(headerSeparated)<2){
					log.Println(r.Header["Host"])
					log.Println("Step 3")
					log.Println(r.URL.String())
					log.Println("**** END ***")
					err = errors.New("Authentication Failed")
					goto ret					
				}

			}
		}

	}else{
		log.Println(r.Header["Host"])
		log.Println("Step 0")
		log.Println(r.URL.String())
		log.Println("**** END ***")
		err = errors.New("No authorization header found")
		goto ret
	}

	passed = true

	ret:
	return passed,err
}

func SetToken(w http.ResponseWriter,userId int,createdAt time.Time)string{
	var payload,accessToken string

	payload = strconv.Itoa(userId)+":"+createdAt.String()
	payload = Utilities.EncryptEntry(payload)+Conf.AdditionalSalt
	
	accessToken = Utilities.EncryptEntry(payload)
	
	Utilities.SetCookie(cookieName,accessToken,w)
	return accessToken
}