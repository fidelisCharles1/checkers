package game

import (
	"errors"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Token struct {
	Player   int    `json:"player"`
	Type     string `json:"type"`
	Position int    `json:"position"`
	Bucket   int    `json:"bucket"`
	Grid     string `json:"grid"`
	Active   bool   `json:"active"`
}

type GameAction struct {
	Player          int              `json:"player"`
	NewState        map[string]Token `json:"state"`
	OpponentName    string           `json:"opponent_name"`
	OpponentPoints  int              `json:"opponent_points"`
	User            int              `json:"user_id"`
	ActionType      string           `json:"action_type"`
	GameSession     int              `json:"session"`
	Winner          int              `json:"winner,omitempty"`
	Turn            int              `json:"player_turn"`
	TargetRecipient string           `json:"target_recipient"`
	Movement        *Movement        `json:"movement,omitempty"`
}

type Movement struct {
	ActivePosition     string                      `json:"active_position,omitempty"`
	Grid               string                      `json:"grid,omitempty"`
	Bucket             int                         `json:"bucket"`
	Position           int                         `json:"position"`
	PossiblePossitions map[string]PossiblePosition `json:"possible_positions"`
}

type PossiblePosition struct {
	Eliminating interface{} `json:"eliminating"`
	Score       int         `json:"score"`
}

type Game struct {
	gorm.Model
	Player1       int    `json:"player_1,omitempty" gorm:"type:int(4);default:null;index"`
	Player2       int    `json:"player2,omitempty" gorm:"type:int(4);default:null;index"`
	Player1Points int    `json:"player_1_points" gorm:"-"`
	Player2Points int    `json:"player_2_points" gorm:"-"`
	Player1Name   string `json:"player_1_name" gorm:"-"`
	Player2Name   string `json:"player_2_name" gorm:"-"`
	GameSession   string `json:"session_id,omitempty" gorm:"type:varchar(15);default:null;index"`
	Winner        int    `json:"game_winner,omitempty" gorm:"type:int(4);default:null;index"`
	Status        string `json:"status,omitempty" gorm:"type:varchar(10);default:'Pending'"`
}

func PrepareModel(db *gorm.DB) {
	if !db.HasTable(&Game{}) {
		db.CreateTable(&Game{})
	}

	//incase there are changes to the schema
	//as per gorm documentation, this does not affect change in column type or removing unwanted collumns
	//https://gorm.io/docs/migration.html
	db.AutoMigrate(&Game{})
}

func (game *Game) Insert(db *gorm.DB) error {
	if db.NewRecord(game) {
		return db.Create(&game).Error
	} else {
		return errors.New("The game could not be created")
	}
}

func (game *Game) Delete(db *gorm.DB) error {
	//since updates only works with non empty values we have to declare separate functions
	//for instances where we need values to be default
	//hence the change status
	return db.Delete(&game).Error
}

func (game *Game) Update(db *gorm.DB) error {
	//since updates only works with non empty values we have to declare separate functions
	//for instances where we need values to be default
	//hence the change status
	return db.Model(&game).Updates(game).Error
}

func (game *Game) ChangeStatus(db *gorm.DB) error {
	return db.Model(&game).Update("status", game.Status).Where("id = ? ", game.ID).Error
}

func GetGame(db *gorm.DB, sessionId string, gameId int) (Game, error) {
	var game Game
	var err error
	tx := db.Select("games.id,games.player1,games.player2,games.game_session,games.status,games.winner,users.name AS player1_name,users1.name AS player2_name").
		Table("games").Joins("LEFT JOIN users ON users.id = games.player1").
		Joins("LEFT JOIN users AS users1 ON users1.id = games.player2")
	if sessionId != "" {
		tx = tx.Where("games.game_session=?", sessionId)
	} else {
		tx = tx.Where("games.id = ?", gameId)
	}

	err = tx.First(&game).Error
	//    err = db.Where("game_session = ?",sessionId).First(&game).Error
	return game, err
}

func (ga *GameAction) DetermineWinnerFromMove() int {
	var winner, player1Tokens, player2Tokens int

	for x := range ga.NewState {
		if ga.NewState[x].Player == 1 {
			player1Tokens++
		} else {
			player2Tokens++
		}
	}

	if player1Tokens == 0 {
		winner = 2
	} else if player2Tokens == 0 {
		winner = 1
	}

	return winner
}
