package users

import (
	"errors"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var UserTypes map[int]string = map[int]string{1: "Admin", 2: "Client"}

const defaultUserColumns = "id,created_at,name,status,updated_by,account_type"

type User struct {
	gorm.Model
	Name         string `json:"name,omitempty" gorm:"type:varchar(100);default:null"`
	LoginKey     string `json:"login_key,omitempty" gorm:"type:varchar(100);index"`
	LoginSecret  string `json:"password,omitempty" gorm:"type:varchar(100)"`
	Channel      string `json:"channel,omitempty" gorm:"type:varchar(50);default:'site'"`
	AccountType  int    `json:"account_type,omitempty" gorm:"type:tinyint(1);default:0"`
	UpdatedBy    int    `json:"updated_by,omitempty" gorm:"type:int(4);default:0"`
	Status       bool   `json:"status,omitempty" gorm:"type:tinyint(4);default:1"`
	Games        int    `json:"games" gorm:"type:int(10);default:0"`
	WeeklyGames  int    `json:"weekly_games" gorm:"type:int(10);default:0"`
	Wins         int    `json:"wins" gorm:"type:int(10);default:0"`
	WeeklyPoints int    `json:"weekly_points" gorm:"type:int(10);default:0"`
	TotalPoints  int    `json:"total_points" gorm:"type:int(10);default:0"`
	Token        string `json:"token,omitempty" gorm:"-"`
	Points       int    `json:"points,omitempty" gorm:"-"`
}

func PrepareModel(db *gorm.DB) {
	if (db.HasTable(&User{}) == false) {
		db.CreateTable(&User{})
	}

	//incase there are changes to the schema
	//as per gorm documentation, this does not affect change in column type or removing unwanted collumns
	//https://gorm.io/docs/migration.html
	db.AutoMigrate(&User{})
}

func (newUser *User) Insert(db *gorm.DB) error {
	if db.NewRecord(newUser) {
		return db.Create(&newUser).Error
	} else {
		return errors.New("The user could not be created")
	}
}

func (user *User) Delete(db *gorm.DB) error {
	//since updates only works with non empty values we have to declare separate functions
	//for instances where we need values to be default
	//hence the change status
	return db.Delete(&user).Error
}

func (user *User) Update(db *gorm.DB) error {
	//since updates only works with non empty values we have to declare separate functions
	//for instances where we need values to be default
	//hence the change status
	return db.Model(&user).Updates(user).Error
}

func (user *User) ChangeStatus(db *gorm.DB) error {
	return db.Model(&user).Update("status", user.Status).Where("id = ? ", user.ID).Error
}

func (user *User) UpdateUserStats(db *gorm.DB) error {
	return db.Exec("UPDATE users SET wins = wins + ?,games = games + ?,weekly_games = weekly_games + ?,"+
		"weekly_points = weekly_points + ?,total_points = total_points + ? WHERE id = ? ",
		user.Wins, user.Games, user.WeeklyGames, user.WeeklyPoints, user.WeeklyPoints, user.ID).Error
}

func GetUsers(db *gorm.DB, accountType int, start int, count int) ([]User, error) {
	var users []User
	var err error

	err = db.Limit(count).Offset(start).Where("account_type = ?", accountType).
		Select(defaultUserColumns + ",channel").
		Order("name").
		Find(&users).Error

	return users, err
}

func SearchUsers(db *gorm.DB, searchTerm string, start int, count int, targetType int) ([]User, error) {
	var users []User
	var err error

	searchTerm = "%" + searchTerm + "%"
	err = db.Limit(count).Offset(start).
		Where(" name LIKE ? AND account_type = ?", searchTerm, targetType).
		Select(defaultUserColumns).
		Order("name").
		Find(&users).Error

	return users, err
}

func GetUser(db *gorm.DB, userId int) (User, error) {
	var user User
	var err error

	err = db.Find(&user, userId).Error
	return user, err
}

func GetUserFromLoginKey(db *gorm.DB, loginKey string) (User, error) {
	var user User
	var err error

	err = db.Where("login_key = ? ", loginKey).
		Select(defaultUserColumns + ",login_secret").
		Find(&user).Error
	return user, err
}

func GetRanking(db *gorm.DB, rankingType string, start, count int) ([]User, error) {
	var users []User
	var err error
	var tx *gorm.DB

	tx = db.Limit(count).Offset(start).
		Select("id,name,weekly_points,total_points,games,weekly_games")

	if rankingType == "weekly" {
		tx = tx.Order("weekly_points DESC")
	} else {
		tx = tx.Order("total_points DESC")
	}

	err = tx.Find(&users).Error

	return users, err
}

func ResetPoints(db *gorm.DB) error {
	var Query string
	Query = "UPDATE users SET weekly_points = 0,weekly_games=0"

	return db.Exec(Query).Error
}
