package Models

import (
        Conf "checkers/Utilities/Conf"
        "github.com/jinzhu/gorm"
        _ "github.com/jinzhu/gorm/dialects/mysql"
    )




func PrepareDatabase()(*gorm.DB,error){
    db, err := gorm.Open("mysql", Conf.ConnectionString)
    if(err!=nil){
        goto ret
    }
    db.DB().SetMaxIdleConns(Conf.MaxIdleConnections)
    db.DB().SetMaxOpenConns(Conf.MaxOpenConnections)

    ret:
    return db,err
}
