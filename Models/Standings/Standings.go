package standings

import (
	Utilities "checkers/Utilities/Utilities"
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Standings struct {
	gorm.Model
	Player     int    `json:"player,omitempty" gorm:"type:int(4);default:null;index"`
	Points     int    `json:"points" gorm:"type:int(4);default:0"`
	PlayerName string `json:"name" gorm:"-"`
	Date       string `json:"date" gorm:"type:varchar(10);default:null;index"`
}

func PrepareModel(db *gorm.DB) {
	if (db.HasTable(&Standings{}) == false) {
		db.CreateTable(&Standings{})
	}

	//incase there are changes to the schema
	//as per gorm documentation, this does not affect change in column type or removing unwanted collumns
	//https://gorm.io/docs/migration.html
	db.AutoMigrate(&Standings{})
}

func (rec *Standings) Insert(db *gorm.DB) error {
	if rec.Date == "" {
		rec.Date = Utilities.GetDate()
	}
	var standing Standings
	var err = db.Where("date = ? AND player = ?", rec.Date, rec.Player).First(&standing).Error
	if err != nil && err.Error() != "record not found" {

		return err
	}

	if standing.Player > 0 {
		standing.Points += rec.Points
		rec = &standing
		return db.Model(&standing).Updates(standing).Error

	} else {
		if db.NewRecord(rec) {
			return db.Create(&rec).Error
		} else {
			return errors.New("The record could not be created")
		}
	}
}

func GetStandings(db *gorm.DB, startDate, endDate string, start, count, specificUser int) ([]Standings, error) {
	var WeekDay map[string]int = map[string]int{"Tuesday": 0, "Wednesday": 1, "Thursday": 2, "Friday": 3, "Saturday": 4, "Sunday": 5, "Monday": 6}

	if startDate == "" {
		weekday := time.Now().Weekday()
		dates := Utilities.IncrementDates(Utilities.GetDate(), WeekDay[weekday.String()])

		if len(dates) > 1 {
			startDate = dates[len(dates)-1]
			endDate = dates[0]
		} else {
			startDate = Utilities.GetDate()
			endDate = startDate
		}

		start = 0
		count = 10
	}

	var standings []Standings
	var err error

	tx := db.Select("users.name AS player_name,SUM(standings.points) AS points,standings.player").
		Table("standings").
		Joins("INNER JOIN users ON users.ID = standings.player").
		Limit(count).Offset(start).Where("standings.date >= ? AND standings.date <= ?", startDate, endDate).
		Order("points DESC").
		Group("standings.player")

	if specificUser != 0 {
		tx = tx.Where("standings.player=?", specificUser)
	}

	err = tx.Scan(&standings).Error

	return standings, err
}
