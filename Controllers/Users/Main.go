package users

import (
	StandingsModel "checkers/Models/Standings"
	UsersModel "checkers/Models/Users"
	Security "checkers/Utilities/Security"
	Utilities "checkers/Utilities/Utilities"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"
)

func PrepareModel(db *gorm.DB) {
	UsersModel.PrepareModel(db)
}

func GetUserTypes(w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "GET" {
		var userTypes map[int]string = UsersModel.UserTypes
		response.Code = 200
		response.Message = ""
		response.Body = userTypes
	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

	fmt.Fprintf(w, response.ToString())
}

func validateUser(db *gorm.DB, userData *UsersModel.User) error {
	var err error
	var user UsersModel.User
	if userData.Name == "" {
		err = errors.New("Name must be filled")
		goto exitCall
	}

	if userData.LoginKey == "" {
		err = errors.New("Login key must be filled")
		goto exitCall
	}

	user, err = UsersModel.GetUserFromLoginKey(db, userData.LoginKey)
	if err != nil {
		if err.Error() == "record not found" {
			err = nil
		} else {
			goto exitCall
		}
	}

	if user.Name != "" && (user.ID != userData.ID) {
		err = errors.New("A user with the given login id already exists")
		goto exitCall
	}

exitCall:
	return err
}

func InsertUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var userData UsersModel.User
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&userData)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = validateUser(db, &userData)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}
		userData.LoginSecret = Utilities.EncryptEntry(userData.LoginSecret)

		err = userData.Insert(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		userData.LoginSecret = ""
		response.Body = userData
		response.Code = 200
		response.Message = "User account added succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func UpdateUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var userData UsersModel.User
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&userData)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = validateUser(db, &userData)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		if userData.LoginSecret != "" {
			userData.LoginSecret = Utilities.EncryptEntry(userData.LoginSecret)
		}

		err = userData.Update(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}
		response.Code = 200
		response.Message = "User account edited succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func DeleteUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var userData UsersModel.User
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&userData)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = userData.Delete(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}
		response.Code = 200
		response.Message = "User account deleted succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func UpdateUserStatus(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var userData UsersModel.User
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&userData)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = userData.ChangeStatus(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}
		response.Code = 200
		response.Message = "User account status edited succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func UpdateUserStats(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var userData UsersModel.User
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&userData)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = userData.UpdateUserStats(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}
		response.Code = 200
		response.Message = "User account stats updated succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func GetUsers(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "GET" {
		var start, count, targetType int
		var search string
		var users []UsersModel.User
		var err error

		if keys, ok := r.URL.Query()["type"]; ok {
			targetType, _ = strconv.Atoi(keys[0])
		}
		if keys, ok := r.URL.Query()["start"]; ok {
			start, _ = strconv.Atoi(keys[0])
		}
		if keys, ok := r.URL.Query()["count"]; ok {
			count, _ = strconv.Atoi(keys[0])
		}
		if keys, ok := r.URL.Query()["search"]; ok {
			search = keys[0]
		}

		if search == "" {
			users, err = UsersModel.GetUsers(db, targetType, start, count)
		} else {
			users, err = UsersModel.SearchUsers(db, search, start, count, targetType)
		}

		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}
		response.Body = users
		response.Code = 200
		response.Message = "User accounts found succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func GetUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "GET" {
		var userId int
		var user UsersModel.User
		var err error
		if keys, ok := r.URL.Query()["userId"]; ok {
			userId, _ = strconv.Atoi(keys[0])
		}

		user, err = UsersModel.GetUser(db, userId)

		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		if user.Name == "" {
			response.Message = "There is no user matching that criteria"
			goto exitCall
		}

		user.LoginSecret = ""
		response.Body = user
		response.Code = 200
		response.Message = "User account found succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func Login(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	Utilities.EnableCors(&w, r)
	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var userData, user UsersModel.User
		var err error
		var standings []StandingsModel.Standings
		decoder := json.NewDecoder(r.Body)
		err = decoder.Decode(&userData)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		if userData.LoginSecret == "" || userData.LoginKey == "" {
			response.Message = "Login id and password must be provided"
			goto exitCall
		}

		user, err = UsersModel.GetUserFromLoginKey(db, userData.LoginKey)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		if user.Name == "" {
			response.Message = "there is no user with the given credentials"
			goto exitCall
		}

		if userData.LoginSecret != Utilities.DecryptEntry(user.LoginSecret) {
			response.Message = "The password you entered is not correct"
			goto exitCall
		}

		if user.Status == false {
			response.Message = "The account you are trying to log into is not active"
			goto exitCall
		}

		user.Token = Security.SetToken(w, int(user.ID), user.CreatedAt)
		standings, err = StandingsModel.GetStandings(db, "", "", 0, 0, int(user.ID))
		if len(standings) > 0 {
			user.Points = standings[0].Points
		}

		user.LoginSecret = ""
		response.Body = user
		response.Code = 200
		response.Message = "Succesfull"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func Resetpoints(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	Utilities.EnableCors(&w, r)
	var response Utilities.APIResponse
	var err error
	response.Code = 415

	err = UsersModel.ResetPoints(db)
	if err != nil {
		response.Message = err.Error()
	} else {
		response.Code = 200
		response.Message = "Points Reset Succesfully"
	}

	fmt.Fprintf(w, response.ToString())
}

func GetRankings(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "GET" {
		var start, count int
		var rankingType string
		var users []UsersModel.User
		var err error

		if keys, ok := r.URL.Query()["rankingType"]; ok {
			rankingType = keys[0]
		}
		if keys, ok := r.URL.Query()["start"]; ok {
			start, _ = strconv.Atoi(keys[0])
		}
		if keys, ok := r.URL.Query()["count"]; ok {
			count, _ = strconv.Atoi(keys[0])
		}

		users, err = UsersModel.GetRanking(db, rankingType, start, count)

		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}
		response.Body = users
		response.Code = 200
		response.Message = "Ranks found succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}
