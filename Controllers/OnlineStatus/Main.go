package OnlineStatus

import (
    Utilities "checkers/Utilities/Utilities"
    Conf "checkers/Utilities/Conf"
    "github.com/allegro/bigcache"
    "encoding/json"
    "net/http"
    "time"
    "fmt"
    "log"
)

func ConfigureCache(cache *bigcache.BigCache)error{
    var err error
    config := bigcache.Config {
        // number of shards (must be a power of 2)
        Shards: Conf.CacheShards,
        // time after which entry can be evicted
        LifeWindow: Conf.CacheLifetime * time.Minute,
        // rps * lifeWindow, used only in initial memory allocation
        MaxEntriesInWindow: Conf.MaxEntriesInCacheWindow,
        // max entry size in bytes, used only in initial memory allocation
        MaxEntrySize: Conf.MaxEntrySize,
        // prints information about additional memory allocation
        Verbose: Conf.VerboseCache,
        // cache will not allocate more memory than this limit, value in MB
        // if value is reached then the oldest entries can be overridden for the new ones
        // 0 value means no size limit
        HardMaxCacheSize: Conf.HardMaxCacheSize,
        // callback fired when the oldest entry is removed because of its expiration time or no space left
        // for the new entry, or because delete was called. A bitmask representing the reason will be returned.
        // Default value is nil which means no callback and it prevents from unwrapping the oldest entry.
        OnRemove: nil,
        // OnRemoveWithReason is a callback fired when the oldest entry is removed because of its expiration time or no space left
        // for the new entry, or because delete was called. A constant representing the reason will be passed through.
        // Default value is nil which means no callback and it prevents from unwrapping the oldest entry.
        // Ignored if OnRemove is specified.
        OnRemoveWithReason: nil,
    }

    cache, err = bigcache.NewBigCache(config)
    if(err!=nil){
        log.Fatal(err)
    }

    return err
}

func UpdateOnlineStatus(cache *bigcache.BigCache,w http.ResponseWriter, r *http.Request){
        var response Utilities.APIResponse
        response.Code = 415
        if( r.Method == "POST" ){
            var err error
            var payload Utilities.CachePayload
            decoder := json.NewDecoder(r.Body)
            err = decoder.Decode(&payload)

            if(err!=nil){
                response.Message = err.Error()
                goto ret
            }

            err = cache.Set(payload.Key, payload.Data)
            if(err!=nil){
                response.Code = 304
                response.Message = err.Error()
                goto ret
            }

            response.Code = 200
            response.Message = "Cache Updated succesfully"


        }else{
            response.Code = 500
            response.Message = "Request method not supported"
        }

       ret:
       fmt.Fprintf(w,response.ToString())

}

func GetOnlineStatus(cache *bigcache.BigCache,w http.ResponseWriter, r *http.Request){
        var response Utilities.APIResponse
        response.Code = 415
        if( r.Method == "GET" ){
            var err error
            var payload Utilities.CachePayload
            if keys, ok := r.URL.Query()["key"]; ok {
                payload.Key = keys[0]
                payload.Data,err = cache.Get(keys[0])

                if(err!=nil){
                    response.Code = 304
                    response.Message = err.Error()
                    goto ret
                }

                response.Code = 200
                response.Message = ""
                response.Body = payload

            }else{
                response.Message = "Request is missing key parameter"
            }

        }else{
            response.Code = 500
            response.Message = "Request method not supported"
        }

       ret:
       fmt.Fprintf(w,response.ToString())

}

func SetOffline(cache *bigcache.BigCache,w http.ResponseWriter, r *http.Request){
        var response Utilities.APIResponse
        response.Code = 415
        if( r.Method == "POST" ){
            var err error
            var payload Utilities.CachePayload
            decoder := json.NewDecoder(r.Body)
            err = decoder.Decode(&payload)

            if(err!=nil){
                response.Message = err.Error()
                goto ret
            }

            err = cache.Delete(payload.Key)
            if(err!=nil){
                response.Code = 304
                response.Message = err.Error()
                goto ret
            }

            response.Code = 200
            response.Message = "Cache Updated succesfully"


        }else{
            response.Code = 500
            response.Message = "Request method not supported"
        }

       ret:
       fmt.Fprintf(w,response.ToString())
}