package standings

import (
	StandangsModen "checkers/Models/Standings"
	Utilities "checkers/Utilities/Utilities"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"
)

func PrepareModel(db *gorm.DB) {
	StandingsModel.PrepareModel(db)
}

func RecordStanding(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var standing StandingsModel.Standings
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&standing)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = standing.Insert(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		response.Body = standing
		response.Code = 200
		response.Message = "Standing Recorded created succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func GetStandings(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "GET" {
		var startDate, endDate string
		var start, count int
		var standings []StandingsModel.Standings
		var err error
		if keys, ok := r.URL.Query()["startDate"]; ok {
			startDate = keys[0]
		}
		if keys, ok := r.URL.Query()["endDate"]; ok {
			endDate = keys[0]
		}
		if keys, ok := r.URL.Query()["start"]; ok {
			start, _ = strconv.Atoi(keys[0])
		}
		if keys, ok := r.URL.Query()["count"]; ok {
			count, _ = strconv.Atoi(keys[0])
		}

		standings, err = StandingsModel.GetStandings(db, startDate, endDate, start, count, 0)

		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		response.Body = standings
		response.Code = 200
		response.Message = "Standings found succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}
