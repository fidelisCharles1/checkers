package games

import (
	Utilities "checkers/Utilities/Utilities"
	WebSocket "checkers/Controllers/WebSockets"
	GamesModel "checkers/Models/Game"
	StandingsModel "checkers/models/Standings"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"
)

func PrepareModel(db *gorm.DB) {
	GamesModel.PrepareModel(db)
}

func CreateGamingSession(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var game GamesModel.Game
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&game)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = game.Insert(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		//setting up the session game session so we return this
		game.GameSession = strconv.Itoa(game.Player1) + "-" + strconv.Itoa(int(game.ID))
		err = game.Update(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		response.Body = game
		response.Code = 200
		response.Message = "Game session created succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func UpdateGamingSession(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var game GamesModel.Game
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&game)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = game.Update(db)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}
		response.Code = 200
		response.Message = "Game session edited succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func GetSession(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "GET" {
		var sessionId string
		var game GamesModel.Game
		var standings []StandingsModel.Standings
		var err error
		if keys, ok := r.URL.Query()["sessionId"]; ok {
			sessionId = keys[0]
		}

		game, err = GamesModel.GetGame(db, sessionId, 0)
		standings, err = StandingsModel.GetStandings(db, "", "", 0, 0, int(game.Player1))
		if len(standings) > 0 {
			game.Player1Points = standings[0].Points
		}

		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		if game.ID == 0 {
			response.Message = "There is no game matching that criteria"
			goto exitCall
		}

		response.Body = game
		response.Code = 200
		response.Message = "Session found succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}

func GameActivity(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	var response Utilities.APIResponse
	response.Code = 415
	if r.Method == "POST" {
		var action GamesModel.GameAction
		var game GamesModel.Game
		var standings StandingsModel.Standings

		var err error
		var bts []byte
		decoder := json.NewDecoder(r.Body)
		err = decoder.Decode(&action)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		bts, err = json.Marshal(action)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		err = WebSocket.Dial(bts, action.TargetRecipient)
		if err != nil {
			response.Message = err.Error()
			goto exitCall
		}

		if action.ActionType == "move" {
			game, _ = GamesModel.GetGame(db, "", action.GameSession)
			fmt.Println(game)

			//winner gets 12 points
			standings.Points = 12
			standings.Player = action.Winner
			standings.Date = Utilities.GetDate()

			standings.Insert(db)

			//loser gets 2 points
			standings.Points = 2
			if game.Player1 == action.Winner {
				standings.Player = game.Player2
			} else {
				standings.Player = game.Player1
			}
			standings.Insert(db)
		}

		response.Code = 200
		response.Message = "action recorded succesfully"

	} else {
		response.Code = 500
		response.Message = "Request method not supported"
	}

exitCall:
	fmt.Fprintf(w, response.ToString())
}
