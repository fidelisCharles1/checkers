package WebSockets

import (
	checkers/Utilities/Conf"
	checkers/Models/Game"
	"encoding/json"
	"flag"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"strings"
)

var addr = flag.String("addr", "localhost:"+Conf.Port, "http service address")
var upgrader = websocket.Upgrader{CheckOrigin: func(r *http.Request) bool {
	return true
}} // use default options
var clients = make(map[*websocket.Conn]string)
var Connections = make(map[int]*websocket.Conn)
var connectionsCount int

type Payload struct {
	Data            interface{} `json:"data"`
	TargetRecipient string      `json:"target_recipient"`
}

func Dial(payload []byte, targetRecipient string) error {
	var err error
	//    var found bool = false

	//    log.Println("Target recipient : "+targetRecipient)
	//    targetRec := strconv.Itoa(targetRecipient)
	for key, val := range clients {
		//        log.Println(val)
		if val == targetRecipient {
			//            log.Println("FOUND")
			//            found = true
			err = key.WriteMessage(websocket.TextMessage, payload)
			if err != nil {
				log.Println(err)
				delete(clients, key)
				if key != nil {
					key.Close()
				}
				continue
			}
		}

	}

	return err
}

func connectionsMessages(iteration int) {
	//mt, message, err := c.ReadMessage()
	_, message, err := Connections[iteration].ReadMessage()
	if err != nil {
		log.Println("read Station:", err)
		handleConnecionError(iteration)
	}

	var gameAction Game.GameAction
	err = json.Unmarshal(message, &gameAction)
	if err != nil {
		log.Println(err)
	}

	/*log.Println(gameAction)
	  log.Println("--------")
	  log.Println(gameAction.DetermineWinnerFromMove())
	*/
	//    dial(message,gameAction.TargetRecipient)
	/*
	    for _ , val := range clients{
	  //      log.Println(val)
	  //      log.Println("---")
	    }
	*/
}

func Connection(w http.ResponseWriter, r *http.Request) {
	var splitString []string
	targetRecipient := r.URL.String()
	targetRecipient = strings.Replace(targetRecipient, "/client-connection/", "", -1)

	//targetRecipient = Utilities.DecryptAccesToken(targetRecipient)
	splitString = strings.Split(targetRecipient, ":")
	if len(splitString) < 2 {
		return
	}
	targetRecipient = splitString[1]

	connection, err := upgrader.Upgrade(w, r, nil)
	if err == nil {
		Connections[connectionsCount] = connection
		clients[connection] = targetRecipient
		go connectionsMessages(connectionsCount)

		//        log.Println(targetRecipient)
		//        log.Println("Added connection")
		//        log.Println("Currently there are "+strconv.Itoa(len(Connections))+" active connections")
		//        log.Println("And there are "+strconv.Itoa(len(clients))+" active clients")

		connectionsCount++
	}

}

func handleConnecionError(iteration int) {
	delete(clients, Connections[iteration])
	if Connections[iteration] != nil {
		Connections[iteration].Close()
	
	 
	 handleConnecionEror(iteration int){
    delete(clients,Connections[iteration])

    if(Connections[iteration]!=nil){
        Connections[iteration].Close()
    }
    delete(Connections,iteration)
    connectionsCount--
}