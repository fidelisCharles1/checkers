package main

import (
	GamesController "checkers/Controllers/Games"
	OnlineStatus "checkers/Controllers/OnlineStatus"
	StandingsController "checkers/Controllers/Standings"
	UsersController "checkers/Controllers/Users"
	WebSockets "checkers/Controllers/WebSockets"
	dbPrep "checkers/Models"
	Conf "checkers/Utilities/Conf"
	Security "checkers/Utilities/Security"
	Utilities "checkers/Utilities/Utilities"
	"log"
	"net/http"

	"github.com/allegro/bigcache"
)

func main() {
	/**/
	db, err := dbPrep.PrepareDatabase()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()

	UsersController.PrepareModel(db)
	StandingsController.PrepareModel(db)
	GamesController.PrepareModel(db)

	/*
	   Caching
	*/
	var cache *bigcache.BigCache

	err = OnlineStatus.ConfigureCache(cache)

	http.HandleFunc("/get-cache", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
			return
		}

		OnlineStatus.GetOnlineStatus(cache, w, r)
	})

	http.HandleFunc("/store-cache", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
			return
		}

		OnlineStatus.UpdateOnlineStatus(cache, w, r)
	})

	http.HandleFunc("/delete-cache", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
			return
		}

		OnlineStatus.SetOffline(cache, w, r)

	})

	/* Users */

	http.HandleFunc("/add-user", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			UsersController.InsertUser(db, w, r)
		}
	})

	http.HandleFunc("/edit-user", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			UsersController.UpdateUser(db, w, r)
		}
	})

	http.HandleFunc("/delete-user", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			UsersController.DeleteUser(db, w, r)
		}
	})

	http.HandleFunc("/edit-user-status", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			UsersController.UpdateUserStatus(db, w, r)
		}
	})

	http.HandleFunc("/get-users", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			UsersController.GetUsers(db, w, r)
		}
	})

	http.HandleFunc("/get-user", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			UsersController.GetUser(db, w, r)
		}
	})

	http.HandleFunc("/create-game-session", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			GamesController.CreateGamingSession(db, w, r)
		}
	})

	http.HandleFunc("/update-game-session", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			GamesController.UpdateGamingSession(db, w, r)
		}
	})

	http.HandleFunc("/game-activity", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			GamesController.GameActivity(db, w, r)
		}
	})

	http.HandleFunc("/get-game-session", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			GamesController.GetSession(db, w, r)
		}
	})

	http.HandleFunc("/record-standings", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			StandingsController.RecordStanding(db, w, r)
		}
	})

	http.HandleFunc("/update-user-stats", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		passed, err := Security.SecurityCheck(r)
		if err != nil || passed == false {
			Utilities.SecurityErrorResponse(&w, err.Error())
		} else {
			UsersController.UpdateUserStats(db, w, r)
		}
	})
	/*
	   These are not protected because we need them to stay open to the public, i.e they do not need authorization
	*/
	http.HandleFunc("/get-user-types", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		UsersController.GetUserTypes(w, r)
	})

	http.HandleFunc("/get-standings", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		StandingsController.GetStandings(db, w, r)
	})

	http.HandleFunc("/admin-login", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		UsersController.Login(db, w, r)
	})

	http.HandleFunc("/reset-points", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		UsersController.Resetpoints(db, w, r)
	})

	http.HandleFunc("/get-rankings", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		UsersController.GetRankings(db, w, r)
	})

	http.HandleFunc("/client-connection/", func(w http.ResponseWriter, r *http.Request) {
		Utilities.EnableCors(&w, r)
		WebSockets.Connection(w, r)
	})

	log.Println(".....Checkers running at port " + Conf.Port + " And securely at port " + Conf.SecurePort)
	if Conf.Live {
		go func() {
			log.Fatal(http.ListenAndServeTLS(":"+Conf.SecurePort, Conf.FullChainFilePath, Conf.PrivateKeyFilePath, nil))
		}()
	}

	log.Fatal(http.ListenAndServe(":"+Conf.Port, nil))
}
