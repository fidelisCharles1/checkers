import React from 'react';
import {APICall,Analytics,LogAnalyticsEvent,logout,detectMobileDevice,numberWithCommas, setSessionStorage, getSessionStorage} from "../API"
import {Menu} from "./menu"
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

//a checkers board contains a matrix of 8x8 squares hece we initialize the inner boxes here
//and complete the box in the setUpBoard function
const row = [0,1,2,3,4,5,6,7]
const bucketEntry = {"position":0,"occupier":null}
const token = {"player":null,"type":"normal","position":null,"bucket":null,"grid":null,"active":false}
const tokenCountPerPlayer = 12

//for AI, we need to assign scores to moves this will allow our AI to make the best possible moves as per the 
//given state of the game
//no gains the move does not gain or eliminate our piece
//gain the move eliminates opponen's piece
//eliminated the move leads to our piece being eliminated
//block the move leads to our piece that was in danger of being eliminated to be spared
//opponentblock the move leads to opponents piece that was in danger of being eliminated to be spared
const scores = {"nogains":0,"gain":100,"becomeKing":50}

const pieceMovedSound = new Audio("./assets/sounds/move.wav");
const eliminatedPieceSound = new Audio("./assets/sounds/became-king.wav")
const winnerFoundSound = new Audio("./assets/sounds/cheers.mp3")
const newKingSound = new Audio("./assets/sounds/trumpet.mp3")

//points assigned to people when they win/or lose
const WinnerPoints = 10
const LoserPoints = 0
const AgainstAIWinnerPoints = 1

const domain = "https://draft.nbcgames.co.tz/"

const basePath = domain+"assets/images/"
var banners = [
    {"banner_path":basePath+"slide.png","banner_link":"https://www.nbc.co.tz/sw/personal/insurance/explore/?utm_source=nbc_checkers"}
]

class Board extends React.Component{
  constructor(props){
  	super(props)

    this.sessionId = window.location.pathname.split("/")
    this.sessionId = this.sessionId[1]

    this.currentTop10 = []
    this.top10Loading = true
    this.socketConnected = false

    this.termsAgreed = false
    this.gameFinnishedLoading = false

    //this is to detect if the same player is trying to play both sides
    this.samePlayerAlert = false
    this.isMobile = detectMobileDevice()

    this.errorType = null
    this.errorMessage = null
    this.bannerLink = null
    this.bannerPath = null

    this.currentWinnersGroup = "weekly"

    this.enviromentSetUp()
    Analytics()

    const uid = getSessionStorage("clientId")
    if( uid !== null && uid !=="" && uid !== undefined && uid !== "null"){
      this.userId = parseInt(uid)
      this.userName = getSessionStorage("clientName")
    }
  }

  clearError = () =>{
    this.errorType = this.errorMessage = null
    this.bannerLink = this.bannerPath = null
    this.forceUpdate()
  }

  //this function handles board initialization
  //i.e the actual drawing of the board along with it's pieces
  boardInitialization = () =>{
    //this is the representation of the board in the system
    this.board = []

    //this is the visual representation of the board
    this.canvas = null

    //digital representation of the game
    this.buckets = []

    //we initialize a varriable to hold no of assigned tokens, we will increment this untill 24,
    // 12 per player during game initializations
    this.assignedTokens = 0

    //we introduce an object data structure to hold game grids and tokens on the given grid,
    //this is so as to reduce the number of loops during game play as object accessing will be at
    //constant time hence improving reaction speeds
    this.gameState = {}    
  }

  enviromentSetUp = () =>{
    this.boardInitialization()
    //this is the numerical session id of the game
    this.receivedSessionId = null

    //which player has to move next
    this.playerTurn = 1

    //what is the current selected token position
    this.activePosition = null

    //current user player i.e if player 1 or 2
    this.me = null

    //are users using the same PC
    this.samePC = false

    //holds the possible grids the next move can be made to
    this.possiblePositions = {}

    //we create an object to hold captures as the game progresses
    this.captures = {"player1_captures":0,"player2_captures":0}

    //this is a flag that allows board updates as moves are made
    //we set this to false when calculating the optimum moves we need to make nxt
    this.allowBoardUpdates = true

    //this lets the system know the opponents are human vs AI, so it can then move on to calculate moves
    this.againstAI = true

    //last known live state is stored on this varriable, 
    //this ensures when the AI calculates the next mpves, it does not affect the real game data
    this.lastGameState = {}

    this.winnerDeclared = false

    //this is to hold the current depth where we no longer need to check moves further
    this.maximumDepth = 5

    //timer section
    this.timer = null
    this.minutes = 0
    this.seconds = 0
    this.gamePaused = true

    this.history = []

    //this is to store how far back in hitory we have gone
    this.historyIteration = 0
    this.allowSounds = true

    //this maintains total seconds the game has been ongoing, 
    //it is useful to send pings to the opponent to let them know you are online
    this.secondsCounter = 0

    //this is the local socket client, it is supposed to be the main socket communications channel
    this.Socket = null

    //this is the current user id
    //only do this if the user is is undefined, otherwise use the same user id we already know
    if(this.userId===undefined){
      this.player1Name = "Player 1"
      this.player2Name = "Player 2"
      this.player1Points = 0
      this.player2Points = 0
      this.userId = null
    }

    //this is to inform where to route the socket messages  
    this.targetRecipient = null

    //this is to let both sides know that the sesison has been matched
    this.matched = false

    //this is to allow us to track when was the last time we received an online flag from the opponent
    this.lastOpponentOnlineTimestamp = 0
    this.opponentOnline = false

    //this is the statement to add to the end of the game
    this.gameEndTitle = ""
    this.gameEndStatement = ""

    //this triggers the opening of the cover to show who won
    this.OpenWinnersCover = false

    this.gameInitialized = false

    //this is the game link to invite a user with
    this.gameLink = "...Inaandaliwa"
    this.opponentSelected = false
  }

  yesRestart = () =>{
      //here we check if this is a sessioned game then we redirect the user to the start page
      if(this.sessionId==="" || this.sessionId===null || this.sessionId === undefined){
        window.top.location = "./"
        return
      }

      if(this.timer!==null){
        clearInterval(this.timer)
      }

      this.updateMoveToOpponent("restart")

      this.enviromentSetUp()
      window.localStorage.removeItem("history")
      this.setUpGameSession()
      this.setUpBoard()
      this.pausePlay()
  }

  restartGame = (forceStart=false) =>{

    if(this.winnerDeclared !== true && forceStart === false){    
      this.Confirm("Unataka kuanza upya?")
    }else{
      this.yesRestart()
    }

  }

  logoutUser = () =>{
    logout()
    this.yesRestart()
  }

  SetUpSocket = () =>{
    this.Socket =  new WebSocket("wss://draft.nbcgames.co.tz:8083/client-connection/"+this.receivedSessionId+"-"+this.userId);


    this.Socket.onopen = () => {
      this.socketConnected = true
    };
    
    this.Socket.onclose = () =>{
      this.socketConnected = false
    }

    this.Socket.onmessage = (message) => {
      let data = JSON.parse(message.data)

      data = data.data

      if(data.player===this.me){
        return
      }

      switch(data.action_type){

        case "move":
          const grid = data.movement.grid
          const position = data.movement.position
          const bucket = data.movement.bucket
          this.activePosition = data.movement.active_position
          this.possiblePositions = data.movement.possible_positions
          this.playerTurn = data.player_turn
          this.gridManipulations(grid,position,bucket)

          //we save history after the latest move has been made
          this.storeHistory()
          this.forceUpdate()
          break
        
        case "token_selection":
          this.gameState = data.state
          this.forceUpdate()
          break

        case "winner":
          this.gameState = data.state
          this.winnerDeclared = true
          this.playSound("winner")
          this.WinnersPopUp(data.winner)
          this.forceUpdate()
          break

        case "restart":
          this.Alert("Mpinzani wako ameweka gemu ianze upya!")
          this.restartGame(true)
          break

        case "matched":
        if(this.matched===false){
            if(this.gameInitialized===false){
              this.initializeGame()
            }
            this.playSound("move")
            this.player2Name = data.opponent_name
            this.player2Points = data.opponent_points
            this.matched = true
            this.targetRecipient = this.receivedSessionId+"-"+data.user_id
            this.forceUpdate()
            let self = this
            this.timer = setInterval(function(){
              self.addTime()
            },1000)          
        }


        break

        default:
        break
    };

  }
}

  setDifficulty = (dificulty) =>{
      this.opponentSelected = true
      this.againstAI = true
      switch(dificulty){
        case "easy":
        this.maximumDepth = 3
        break
        case "medium":
        this.maximumDepth = 4
        break
        case "hard":
        this.maximumDepth = 5
        break
        default:
        this.againstAI = false
        this.maximumDepth = 1
        let elements = document.getElementsByClassName('levelSelector');
        for(let x = 0 ; x < elements.length ; x++){
          elements[x].checked = false
        }

      }

  }

  storeHistory = () =>{
    if(this.history.length>5){
        this.history.splice(0,1)
    }
    const historyState = {"turn":this.playerTurn,"state":{...this.gameState},"captures":{...this.captures},"minutes":this.minutes,"seconds":this.seconds}
    this.history.push(historyState)
    this.historyIteration = 0
    this.playSound("move")
    window.localStorage.setItem("history",JSON.stringify(this.history))
  }

  playSound = (sound) =>{
      if(this.allowSounds===false || this.allowBoardUpdates===false){
        return 
      }

      try{

          switch(sound){
            case "move":
              pieceMovedSound.play()
              break
            case "eliminate":
              eliminatedPieceSound.play()
              break
            case "winner":
              //we are adding a delay here so we do not have 2 sounds playing at the same time
              setTimeout(function(){
                winnerFoundSound.play()
              },300)
              break
            case "newKing":
              newKingSound.play()
            break
            default:
          }

      }catch(e){
        console.log(e)
      }

  }

  toggleSounds = () =>{
    this.allowSounds = !this.allowSounds
    this.forceUpdate()
  }

  goBack = () =>{
      if(this.history.length===0 || this.winnerDeclared){
        return
      }

      const index = (this.history.length-2)-this.historyIteration
      if(index<0){
        return 
      }

      this.gameState = {...this.history[index].state}
      this.captures = {...this.history[index].captures}
      this.playerTurn = this.history[index].turn

      for(let [key] of Object.entries(this.gameState)){
        if(this.gameState[key].active===true){
            this.gameState[key].active = false
            break
        }
      }


      this.historyIteration++
      this.forceUpdate()
  }

  continueWithGame = () =>{
    if(this.playerTurn===2 && this.againstAI===true){
      this.allowBoardUpdates = false
    }
    this.AITurn()
  }

  addTime = () =>{
    if(this.winnerDeclared===true){
      window.localStorage.removeItem("history")
      window.clearInterval(this.timer)
      return
    }

    if(this.againstAI===false && this.socketConnected===false && this.gameInitialized===true){
      //if connection failed, retry again
      this.SetUpSocket()
    }

    if(!this.gamePaused){
        this.seconds++
        if(this.seconds>59){
          this.seconds = 0
          this.minutes++
        }

        this.forceUpdate()
    }
    this.updateOnlineStatus()
  }

  pausePlay = () =>{
    if(this.gamePaused===false){
      this.gamePaused = true
      //we dont clear interval because we need this to update online status to the server
    }else{
      this.gamePaused = false
      //if timer is not null means the game had already started and hence we do not need to reset it, 
      //we just release the pause flag
      if(this.timer===null){
          let self = this
          this.timer = window.setInterval(function(){
              self.addTime()
          },1000)           
      }
    }
    this.forceUpdate()
  }

  closeWinnersCover = () =>{
      this.OpenWinnersCover = false
      this.forceUpdate()
  }

  WinnersPopUp = (declaredWinner) =>{
      this.OpenWinnersCover = true
      let title = <h1>HONGERA</h1>
      //
      let points = WinnerPoints
      let iWon = true 
      if(this.againstAI===true){
        points = AgainstAIWinnerPoints
      }
      let message = "Umeshinda kwa point "
      if(declaredWinner!==this.me){
        title = <h1>HAUJAFANIKIWA KUSHINDA</h1>
        message = "Umepata point "
        points = LoserPoints
        iWon = false
      }

      //
      if(iWon){
        if(this.me===1){
          this.player1Points += points 
          this.player2Points += LoserPoints 
        }else{
          this.player2Points += points 
          this.player1Points += LoserPoints 
        }
      }else{
        if(this.me===1){
          this.player1Points += LoserPoints
          this.player2Points += points 
        }else{
          this.player2Points += LoserPoints
          this.player1Points += points 
        }        
      }

      if(this.againstAI){
        const record = {
          "player":this.userId,
          "points":parseInt(this.points)
        }
        APICall("record-standings",record,"POST");
      }


      const tmpUser = {
        "id":this.userId,
        "weekly_points":parseInt(points),
        "points":parseInt(points),
        "wins":iWon===true ? 1 : 0
      }
      APICall("update-user-stats",tmpUser,"POST");

      message += points
      this.gameEndTitle = title
      this.gameEndStatement = <h2>{message}</h2>
      //
      LogAnalyticsEvent("In Game Activity","Winner Declared",null,false)
  }

  //here we use this to mark a captured token by the active player
  //we can also begin the preliminary checks of game winner here i.e
  //if a respective player has captured 12 pieces then they are the winner of the said round
  incrementPlayerCaptures = (player) =>{
      if(this.allowBoardUpdates===false){
        return
      }

      switch(player){
          case 1:
          this.captures.player1_captures++
          this.playSound("eliminate")
          if(this.captures.player1_captures===12){
            this.playSound("winner")
            this.winnerDeclared = true
            this.WinnersPopUp(1)
            this.updateMoveToOpponent("winner",null,1)
          }
          break
     
          default:
          this.captures.player2_captures++
          this.playSound("eliminate")
          if(this.captures.player2_captures===12){
            this.playSound("winner")
            this.winnerDeclared = true
            this.WinnersPopUp(2)
            this.updateMoveToOpponent("winner",null,2)
          }
          break
      }
  }

  
  componentDidMount(){
    window.localStorage.removeItem("history")
    if(this.sessionId===""){
      this.setUpGameSession()
    }
    else{
      this.getGameSession()
    }
    this.getTop10()
    this.setUpBoard()
    this.pausePlay()
    this.gameFinnishedLoading = true
  }

  assignToken = (row,position) =>{

      let tempToken = {}
      tempToken = Object.assign(tempToken,token)
      tempToken.bucket = row
      tempToken.position = position
      tempToken.grid = row+""+position

      if(this.assignedTokens<12){
        tempToken.player = 1
      }else{
        tempToken.player = 2      
      }

      this.gameState[tempToken.grid] = tempToken
      
      this.assignedTokens++
  }

  inititalizeTokenPlacement = () =>{

  	//player1
    //this player occupies areas closest to the bottom of the screen(without rotating the board)
    //hence we start filling positions at row 7 going to row 5
    let startRow = 7
    let currentRow = 0
    let rowsFilled = 0
    let tokensFilled = 0
    while(tokensFilled < tokenCountPerPlayer){
        currentRow = startRow - rowsFilled
        if(currentRow%2){

          for(let y = 0 ; y < row.length ; y+=2){
            this.assignToken(currentRow,y)
              tokensFilled++
          }          

        }else{

          for(let y = row.length - 1 ; y >= 0 ; y-=2){
              this.assignToken(currentRow,y)
              tokensFilled++
          }          

        }

        rowsFilled++
  	}


    //player2
    //this player occupies areas furthest to the bottom of the screen(without rotating the board)
    //hence we start filling positions at row 0 going to row 2
    startRow = 0
    currentRow = 0
    rowsFilled = 0
    tokensFilled = 0
    while(tokensFilled < tokenCountPerPlayer){

        currentRow = startRow + rowsFilled

        if(currentRow%2){

          for(let y = 0 ; y < row.length ; y+=2){
            tokensFilled++
            this.assignToken(currentRow,y)
          }          

        }else{

          for(let y = 1 ; y < row.length ; y+=2){
            tokensFilled++
            this.assignToken(currentRow,y)
          }          

        }

        rowsFilled++

    }

  }

  fillBucket = (index) =>{
  	let bucketPositions = []
  	for(let x = 0 ; x < 8 ; x++){
  		let tmp = {...bucketEntry}
  		tmp.position = x
  		bucketPositions.push([index,tmp])
  	}
	 
    this.buckets.push(bucketPositions)
  }

  placeToken = (gridPos) =>{

    if(this.winnerDeclared || this.gamePaused ){
      return
    }

    let token = null
    if(this.gameState[gridPos]===undefined){
      return token
    }

    switch( parseInt( this.gameState[gridPos].player ) ){
      case 1:
      token = <i className={"dark token player1 " +(this.gameState[gridPos].active===true ? "active " : " " ) + this.gameState[gridPos].type } 
                  gridpos={gridPos} 
                  onClick={this.selectPosition}>
              </i>
      //
      break
      case 2:
      token = <i className={"dark token player2 "+(this.gameState[gridPos].active===true ? "active " : " " ) + this.gameState[gridPos].type} 
                 gridpos={gridPos} 
                 onClick={this.selectPosition}>
              </i>
      //
      break

      default:
      break
    }

    return token
  }

  fillSegment = (positions,segment) =>{
    let dark = true
    if(segment%2){
      dark = false
    }
    const segments = positions.map((row,index)=>{
      dark = !dark
      return <div className={"position "+(dark ? "dark" : "light")} onClick={this.selectPosition} databucket={segment} dataindex={index} gridpos={segment+""+index} key={index}>
        <p className="hidden redText">( {segment+","+index} )</p>

        { 
          dark===true ? 
          this.placeToken(segment+""+index) : 
          null
        }
      </div>
    })
    //
    return segments
  }


  setUpBoard = () =>{
    this.inititalizeTokenPlacement()

    for(let x = 0; x < 8 ; x++){
      this.board.push(row)
      this.fillBucket(x)
    }

    let tmp = window.localStorage.getItem("history")
    if(tmp!==null){
      this.history = JSON.parse(tmp)

      const index = this.history.length - 1
      this.gameState = this.history[index].state
      this.captures = {...this.history[index].captures}
      this.playerTurn = this.history[index].turn
      this.minutes = this.history[index].minutes
      this.seconds = this.history[index].seconds

      for(let [key] of Object.entries(this.gameState)){
        if(this.gameState[key].active===true){
            this.gameState[key].active = false
            break
        }
      }

    }

    this.storeHistory()
    this.forceUpdate()
  }

  /*
    This is the actual grid changes we want to apply
    returns the winner after the move or null if none
  */
  gridManipulations = (gridPos,position,bucket) =>{
      const tmpGrid = bucket+""+position
      let justBecameKing = false

      this.gameState[gridPos] = {...this.gameState[this.activePosition]}
      this.gameState[gridPos].position = position 
      this.gameState[gridPos].bucket = bucket 
      this.gameState[gridPos].active = false
      this.gameState[gridPos].grid = tmpGrid

      if(this.gameState[gridPos].player===1 && bucket===0){
          if(this.gameState[gridPos].type!=="king"){
              this.playSound("newKing")
              this.gameState[gridPos].type="king"
              justBecameKing = true
          }
      }else if(this.gameState[gridPos].player===2 && bucket===7){
          if(this.gameState[gridPos].type!=="king"){
              this.playSound("newKing")
              this.gameState[gridPos].type="king"            
              justBecameKing = true
          }
      }

      delete this.gameState[this.activePosition]


      if( this.possiblePositions[tmpGrid].eliminating!==false ){
        this.incrementPlayerCaptures(this.playerTurn)
        delete this.gameState[this.possiblePositions[tmpGrid].eliminating]


        //since the token has captured an opponent's token then we allow them to move again if there is an 
        //opportunity to move and capture another opponent's token
        //unless they just became king
        if( justBecameKing ){
          this.playerTurn = this.playerTurn === 1 ? 2 : 1
          return null
        }

        let positionsDeleted = 0
        this.possiblePositions = this.tokenCanMove(this.gameState[gridPos],false)

        let totalPositionsFound = Object.keys(this.possiblePositions).length
        if(totalPositionsFound !== 0 && this.possiblePositions.constructor === Object){

            //we are removing all positions that will not lead to our active player to eliminate an opponent's 
            //token, we determine these positions because the value will be false instead of the gridpos mark
            for (let [key,val] of Object.entries(this.possiblePositions)) {
                if(val.eliminating===false){
                    positionsDeleted++
                    delete this.possiblePositions[key]
                }

            }

            //if we have not deleted all positions from the possible list
            //we then have the poitions Deleted not equal to totalpositions found

            if(positionsDeleted !== totalPositionsFound){
                this.gameState[gridPos].active = true
                this.activePosition = gridPos
                return null

            }
        }


      }

      if( this.winnerDeclared === false ){

            this.playerTurn = this.playerTurn === 1 ? 2 : 1

            if( this.canThePlayerMove(this.playerTurn) === true ){

                //////////////
                if(this.samePC){
                    this.me = this.playerTurn
                }
                //////////////

                this.activePosition = null

            }else{

              //we are swapping this as we found out the opponent can not move hence the user who didi the last move wins
              this.playerTurn = this.playerTurn === 1 ? 2 : 1
              if(this.allowBoardUpdates){
                  this.winnerDeclared = true
                  this.playSound("winner")
                  this.WinnersPopUp(this.playerTurn)
                  this.updateMoveToOpponent("winner",null,this.playerTurn)
              }else{
                return this.playerTurn
              }
            }

      }



      return null
  }

  traverseNode = (node, depth) =>{

      let maxScore = -10000

      if(Object.entries(node.moves).length>0){
          for(let [key] of Object.entries(node.moves)){
              maxScore = -10000
              for(let [key1] of Object.entries(node.moves[key])){

                  const tmp = this.traverseNode(node.moves[key][key1],depth+1)

                  if( tmp > maxScore ){
                      maxScore = tmp
                  }

              }

              switch(node.player){
                case 1:
                    node.score = ( node.score + maxScore ) /  depth
                  break

                case 2:
                    node.score = ( node.score - maxScore ) / depth
                break

                default:
                break
              }

              //because we are enforcing elimination(i.e available eliminations must be performed)
              //we force score to be positive in the case of possible elimination
              //multily by 2 to add more emphasis
              if(node.score===0 && node.eliminating !== false){
                node.score = node.player === 1 ? -1 * (2* scores.gain) : (2 * scores.gain )
              }


          }
      }


      return node.score / depth
  }

  //function to select best move from possible moves, we want the move with maximum no of points in our favor
  selectOptimumMove = (moves) =>{
    let selectedMove = {}
    let currentMax = -100000
    let movesWithMatchingScores = []

    for(let [key] of Object.entries(moves)){
        //here we traverse all nodes and adjust scores accordingly
        for( let [key1] of Object.entries( moves[key] ) ){
            this.traverseNode( moves[key][key1] , 1 )
        }
    }

    for(let [key] of Object.entries(moves)){
        //here we traverse all nodes and concider scores to adjust our next move
        for( let [key1] of Object.entries( moves[key] ) ){

            if(moves[key][key1].score > currentMax){

              currentMax = moves[key][key1].score 
              const move = {"from":key,"to":key1,"score":moves[key][key1].score,"eliminating":moves[key][key1].eliminating}

              this.addEliminatingMove(movesWithMatchingScores,move,true)

            }else if(moves[key][key1].score === currentMax){

              const move = {"from":key,"to":key1,"score":moves[key][key1].score,"eliminating":moves[key][key1].eliminating}
              this.addEliminatingMove(movesWithMatchingScores,move,false)

            }

        }
    }

    /*
    console.log(moves)
    console.log("------")
    console.log(movesWithMatchingScores)
    */

    //get random move
    const item = movesWithMatchingScores[Math.floor(Math.random() * movesWithMatchingScores.length)];
    selectedMove["from"] = item.from
    selectedMove["to"] = item.to

    return selectedMove
  }

  addEliminatingMove = (interestingMoves,move,newScore = false) =>{
      //if the new move does not eliminate a token but we already have an eliminating move we just return
      if(move.eliminating===false){
          if(interestingMoves.length > 0 && interestingMoves[0].eliminating!==false){
              return
          }
      }else{
        //if new move eliminates elements then we see from the list of possible moves if there are non eliminating
        //moves and we remove them from contention
        for(let x = 0 ; x < interestingMoves.length ; x++){
            if(interestingMoves[x].eliminating===false){
              interestingMoves.splice(x,1)
            }
        }
      }



      if(newScore){
        interestingMoves.length = 0
      }

      interestingMoves.push(move)
  }

  AITurn = () =>{
      this.lastGameState = {...this.gameState}

      let self = this
      const currentPlayerTurn = this.playerTurn

      setTimeout(function(){

          //only enter here if against AI
          if( self.againstAI ===true ){
            //only enter here if player is 2 or player 1 with allow updates equal to false
            if( self.playerTurn === 2 ){
                let moves = self.getAllPossiblePlayerMoves(1)
                self.gameState = self.lastGameState
                self.allowBoardUpdates = true
                self.playerTurn = currentPlayerTurn
                const move = self.selectOptimumMove(moves)
                self.AIMove(move.from,move.to)
            }
          }


      },200)    
  }

  AIMove = (from,to,backwardsAllowed = false) =>{
        this.activePosition = from
        const position = parseInt(to.substring(1,2))
        const bucket = parseInt(to.substring(0,1))
        this.possiblePositions = this.tokenCanMove(this.gameState[from],backwardsAllowed)

        this.gridManipulations(to,position,bucket)
        this.storeHistory()

        if(Object.entries(this.possiblePositions).length>0 && this.playerTurn===2){
          this.forceUpdate()
          for(let [key] of Object.entries(this.possiblePositions)){
              this.AIMove(this.activePosition,key,true)
              this.storeHistory()
              break
          }
        }

        this.forceUpdate()
  }

  /*
    This is called when the user actually moves pieces around
  
  */

  placeTokenAtPosition = (gridPos,e) =>{
      if(this.gamePaused){
        return
      }

      const position = parseInt(e.target.getAttribute("dataindex"))
      const bucket = parseInt(e.target.getAttribute("databucket"))
      const tmpGrid = bucket+""+position

      if(this.possiblePositions[tmpGrid] === undefined ){
        this.Alert("Hauwei kucheza hapo!")
        return
      }

      let canEliminate = this.canThePlayerMove(this.playerTurn,true)

      if( canEliminate === true && this.possiblePositions[tmpGrid].eliminating===false){
      	this.Alert("Hauwezi kucheza sehemu nyingine kama kuna kete ina nafasi ya kula")
      	return
      }

      if(this.possiblePositions[tmpGrid]===undefined){
          this.Alert("Hauwezi kucheza hapo");
          return
      }

      if(this.activePosition===null){
          this.Alert("Chagua kitufe kwanza")
          return
      }

      if(this.gameState[this.activePosition].player!==this.playerTurn){
          this.Alert("Unaruhusiwa kuhamisha kitufe chako peke yake")
          return
      }

      this.updateMoveToOpponent("move",gridPos)


      this.gridManipulations(gridPos,position,bucket)
      this.storeHistory()

      this.AITurn()
  }

  selectToken = (gridPos) =>{
      if(this.matched===false){
        this.Alert("Tunamsubiria rafiki ajiunge")
        return
      }
      if(this.winnerDeclared || this.gamePaused){
        return
      }

      if(this.me !== this.gameState[gridPos].player){
        this.Alert("Sio kitufe chako")
        return
      }

      if(this.gameState[this.activePosition]!==undefined){
          this.gameState[this.activePosition].active = false
      }
      this.possiblePositions = {}

      this.gameState[gridPos].active = true
      this.activePosition = gridPos
      this.possiblePositions = this.tokenCanMove(this.gameState[gridPos])
      this.updateMoveToOpponent("token_selection")
  }


  selectPosition = (e) =>{
    e.stopPropagation()

    if(e.target.classList.contains("dark")===false){
      this.Alert("Hauwezi kucheza hapo")
      return
    }

    if( this.me !== this.playerTurn ){
      //+this.me+" - "+this.playerTurn
      this.Alert("Sio zamu yako kucheza ")
      return
    }

    const gridPos = e.target.getAttribute("gridpos")

    if(this.gameState[gridPos]===undefined){

        this.placeTokenAtPosition(gridPos,e)

    }else{
        this.selectToken(gridPos)
    }

    this.forceUpdate()
  }

  isKing = (token) =>{
    try{
        return token.type === "king"
    }catch(e){
      return false
    }
  }

  //this function tries to determine possible oves for the king token,
  //basically the king can move in 4 different general directions :-
  //TL,TR,BL,BR (Top Left,Top Right,Bottom Left, Bottom Right)
  //we will check each dirrection
  getKingPossibleMoves = (startBucket,startPosition,player) =>{
      let possiblePositions = {} 
      let grid = null
      let gridOccupier = null
      let gain = scores.nogains

      //we use this to mark that we already have a locked token in our scope
      //this is especially true for situations where we have a king who can move
      //and eliminate a token, by the rules of the game the king can oly eliminate 1 
      //opponent token with one movement(single block or multiple blocks) hop
      let lockedToken = false
      

      //check Top Left direction
      //here we start by deducting the numbers as we want to use a stage top left of the current 
      //start position and bucket
      let bucket = startBucket - 1
      let position = startPosition - 1

      let tempPosition = {}
      while(bucket >= 0 && position >= 0){

          grid = bucket+""+position
          gridOccupier = this.positionOccupied(grid)

          if(gridOccupier===null){
            tempPosition = {}
            tempPosition[grid] = {"eliminating":lockedToken,"score":gain}
            possiblePositions = Object.assign(possiblePositions,tempPosition)

          }else if(gridOccupier===player){
            //here we find out that the position we landed has the same player's token
            //hence can not proceed further, we stop right here
            break
          }else{
            //we check if we already have an opponent's token in our scope
            if(lockedToken!==false){
              break
            }
            gain = scores.gain

            //here we determined that the token found is not our players, hence we check one step
            //further to determine if empty or not, if empty we can move there otherwise we stop here
            const extraBucket = bucket-1
            const extraPos = position-1
            if(extraBucket>=0 && extraPos>=0){

                const extraGrid = extraBucket+""+extraPos
                if(this.positionOccupied(extraGrid)===null){
                  tempPosition = {}
                  tempPosition[extraGrid] = {"eliminating":grid,"score":gain}
                  lockedToken = grid
                  possiblePositions = Object.assign(possiblePositions,tempPosition)

                  bucket = extraBucket
                  position = extraPos
                  continue
                }

            }

            break
          }
          bucket--
          position--
      }


      //check Top Right direction
      bucket = startBucket - 1
      position = startPosition + 1
      lockedToken = false
      gain = scores.nogains

      while(bucket >= 0 && position <= 7){

          grid = bucket+""+position
          gridOccupier = this.positionOccupied(grid)

          if(gridOccupier===null){
            tempPosition = {}
            tempPosition[grid] = {"eliminating":lockedToken,"score":gain}
            possiblePositions = Object.assign(possiblePositions,tempPosition)

          }else if(gridOccupier===player){
            //here we find out that the position we landed has the same player's token
            //hence can not proceed further, we stop right here
            break
          }else{
            //we check if we already have an opponent's token in our scope
            if(lockedToken!==false){
              break
            }
            gain = scores.gain

            //here we determined that the token found is not our players, hence we check one step
            //further to determine if empty or not, if empty we can move there otherwise we stop here
            const extraBucket = bucket-1
            const extraPos = position+1
            if(extraBucket>=0 && extraPos<=7){

                const extraGrid = extraBucket+""+extraPos
                if(this.positionOccupied(extraGrid)===null){
                  tempPosition = {}
                  tempPosition[extraGrid]={"eliminating":grid,"score":gain}
                  lockedToken = grid
                  possiblePositions = Object.assign(possiblePositions,tempPosition)

                  bucket = extraBucket
                  position = extraPos
                  continue

                }

            }

            break
          }
          bucket--
          position++
      }


      //check Bottom Left direction
      bucket = startBucket + 1
      position = startPosition - 1
      lockedToken = false
      gain = scores.nogains

      while(bucket <= 7 && position >= 0){

          grid = bucket+""+position
          gridOccupier = this.positionOccupied(grid)

          if(gridOccupier===null){
            tempPosition = {}
            tempPosition[grid] = {"eliminating":lockedToken,"score":gain}
            possiblePositions = Object.assign(possiblePositions,tempPosition)

          }else if(gridOccupier===player){
            //here we find out that the position we landed has the same player's token
            //hence can not proceed further, we stop right here
            break
          }else{
            //we check if we already have an opponent's token in our scope
            if(lockedToken!==false){
              break
            }

            gain = scores.gain
            //here we determined that the token found is not our players, hence we check one step
            //further to determine if empty or not, if empty we can move there otherwise we stop here
            const extraBucket = bucket+1
            const extraPos = position-1
            if(extraBucket<=7 && extraPos>=0){

                const extraGrid = extraBucket+""+extraPos
                if(this.positionOccupied(extraGrid)===null){
                  tempPosition = {}
                  tempPosition[extraGrid]={"eliminating":grid,"score":gain}
                  lockedToken=grid
                  possiblePositions = Object.assign(possiblePositions,tempPosition)

                  bucket = extraBucket
                  position = extraPos
                  continue
                }

            }

            break
          }
          bucket++
          position--
      }

      //check Bottom Right direction
      bucket = startBucket + 1
      position = startPosition + 1
      lockedToken = false
      gain = scores.nogains

      while(bucket <= 7 && position <= 7){

          grid = bucket+""+position
          gridOccupier = this.positionOccupied(grid)

          if(gridOccupier===null){
            tempPosition = {}
            tempPosition[grid] = {"eliminating":lockedToken,"score":gain}
            possiblePositions = Object.assign(possiblePositions,tempPosition)

          }else if(gridOccupier===player){
            //here we find out that the position we landed has the same player's token
            //hence can not proceed further, we stop right here
            break
          }else{
            //we check if we already have an opponent's token in our scope
            if(lockedToken!==false){
              break
            }

            gain = scores.gain            
            //here we determined that the token found is not our players, hence we check one step
            //further to determine if empty or not, if empty we can move there otherwise we stop here
            const extraBucket = bucket + 1
            const extraPos = position + 1
            if(extraBucket<=7 && extraPos<=7){

                const extraGrid = extraBucket+""+extraPos
                if(this.positionOccupied(extraGrid)===null){
                  tempPosition = {}
                  tempPosition[extraGrid]={"eliminating":grid,"score":gain}
                  lockedToken =grid
                  possiblePositions = Object.assign(possiblePositions,tempPosition)

                  bucket = extraBucket
                  position = extraPos
                  continue
                }

            }

            break
          }
          bucket++
          position++
      }

      return possiblePositions
  }

  //this function returns null,1 or 2 indicating if position given by the gridpos parameter 
  //is occupied
  positionOccupied = (gridpos) =>{
      if( this.gameState[gridpos] === undefined ){
          return null
      }

      return this.gameState[gridpos].player
  }

  //returns -1 if not applicable
  rightShift = (bucket,finalPos) =>{

    if( finalPos <= 7 ){
        const grid = bucket+""+finalPos
        return this.positionOccupied(grid)
    }

    return -1
  }

  //returns -1 if not applicable
  leftShift = (bucket,finalPos) =>{

    if( finalPos >=0 ){
        const grid = bucket+""+finalPos
        return this.positionOccupied(grid)
    }

    return -1
  }

  leftRightMovement = (token,bucket,player,forcePlus=null) => {
      let possibleMoves = {}
      let tmpPosition = null
      let tmpOccupier = null
      let grid = null

      switch(forcePlus){
          case 1:

              if(player===1){
                  bucket-=1

                  if(bucket<0){
                    return possibleMoves
                  }

              }else{
                  bucket+=1

                  if(bucket>7){
                    return possibleMoves
                  }

              }


              tmpPosition = token.position + 1
              grid = bucket+""+tmpPosition

              tmpOccupier = this.rightShift(bucket,tmpPosition)
              if(tmpOccupier===null){
                possibleMoves[grid] = {"eliminating":false,"score":scores.nogains}
              }

          break

          case -1:

              if(player===1){
                  bucket-=1

                  if(bucket<0){
                    return possibleMoves
                  }

              }else{
                  bucket+=1

                  if(bucket>7){
                    return possibleMoves
                  }

              }


              tmpPosition = token.position - 1
              grid = bucket+""+tmpPosition

              tmpOccupier = this.leftShift(bucket,tmpPosition)
              if(tmpOccupier===null){
                possibleMoves[grid] = {"eliminating":false,"score":scores.nogains}
              }

          break

          default:

              //increment position
              tmpPosition = token.position + 1
              grid = bucket+""+tmpPosition
              tmpOccupier = this.rightShift(bucket,tmpPosition)

              if(tmpOccupier===null){
                possibleMoves[grid] = {"eliminating":false,"score":scores.nogains}
              }else if(tmpOccupier!==player && tmpOccupier!==-1){

                let t = this.leftRightMovement(this.gameState[grid],bucket,player,1)
                if(Object.keys(t).length !== 0 && t.constructor === Object){
                  for (let [key] of Object.entries(t)) {
                    t[key] = {"eliminating":grid,"score":scores.gain}
                  }
                  possibleMoves = Object.assign(possibleMoves,t)
                }

              }

              tmpPosition = token.position - 1
              grid = bucket+""+tmpPosition
              tmpOccupier = this.leftShift(bucket,tmpPosition)
              if(tmpOccupier===null){
                possibleMoves[grid] = {"eliminating":false,"score":scores.nogains}
              }else if(tmpOccupier!==player && tmpOccupier!==-1){

                let t = this.leftRightMovement(this.gameState[grid],bucket,player,-1)
                if(Object.keys(t).length !== 0 && t.constructor === Object){
                  for (let [key] of Object.entries(t)) {
                    t[key] = {"eliminating":grid,"score":scores.gain}
                  }
                  possibleMoves = Object.assign(possibleMoves,t)
                }

              }

      }

        return possibleMoves
  }

  //this is to facilitate the use case where a player can move backwards and capture another token
  //after it has just captured another, this leads to a zig zag pattern and hence why we separate it 
  //from normal play,
  //What we do here is check the position directly behind our token of interested and determine if there is
  //a token and if the token is an opponents, then we check one position beyond that to see if it is open 
  checkBackMovementCompatibility = (token) =>{
      let possibleMoves = {}
      let tmpBucket = null
      let tmpPosition = null
      let occupier = null

      //this is used to determine which grid to eliminate when a move is done
      let tmpGrid = null
      if(token.player===1){

          if(token.bucket<7){

              //left check
              tmpBucket = token.bucket + 1
              tmpPosition = token.position - 1
              occupier = this.leftShift(tmpBucket,tmpPosition)

              //position is not empty and is occupied by player 2 hence we go 1 step further
              if(occupier!==null && occupier === 2){

                  if(tmpBucket<7){
                      tmpGrid = tmpBucket+""+tmpPosition
                      //move a step further
                      tmpBucket += 1
                      tmpPosition -= 1

                      occupier = this.leftShift(tmpBucket,tmpPosition)
                      if(occupier===null){
                         possibleMoves[tmpBucket+""+tmpPosition] = {"eliminating":tmpGrid,"score":scores.gain}
                      }
                  }

              }


              //right check
              tmpBucket = token.bucket + 1
              tmpPosition = token.position + 1
              occupier = this.rightShift(tmpBucket,tmpPosition)

              //position is not empty and is occupied by player 2 hence we go 1 step further
              if(occupier!==null && occupier === 2){

                    if(tmpBucket<7){
                        tmpGrid = tmpBucket+""+tmpPosition
                        //move a step further
                        tmpBucket += 1
                        tmpPosition += 1

                        occupier = this.rightShift(tmpBucket,tmpPosition)
                        if(occupier===null){
                         possibleMoves[tmpBucket+""+tmpPosition] = {"eliminating":tmpGrid,"score":scores.gain}
                        }
                    }

              }

          }

      }else{


          if(token.bucket>0){

              //left check
              tmpBucket = token.bucket - 1
              tmpPosition = token.position - 1
              occupier = this.leftShift(tmpBucket,tmpPosition)

              //position is not empty and is occupied by player 2 hence we go 1 step further
              if(occupier!==null && occupier === 1){

                  if(tmpBucket>0){
                      tmpGrid = tmpBucket+""+tmpPosition
                      //move a step further
                      tmpBucket -= 1
                      tmpPosition -= 1

                      occupier = this.leftShift(tmpBucket,tmpPosition)
                      if(occupier===null){
                         possibleMoves[tmpBucket+""+tmpPosition] = {"eliminating":tmpGrid,"score":scores.gain}
                      }
                  }

              }


              //right check
              tmpBucket = token.bucket - 1
              tmpPosition = token.position + 1
              occupier = this.rightShift(tmpBucket,tmpPosition)

              //position is not empty and is occupied by player 2 hence we go 1 step further
              if(occupier!==null && occupier === 1){

                    if(tmpBucket>0){
                        tmpGrid = tmpBucket+""+tmpPosition
                        //move a step further
                        tmpBucket -= 1
                        tmpPosition += 1

                        occupier = this.rightShift(tmpBucket,tmpPosition)
                        if(occupier===null){
                         possibleMoves[tmpBucket+""+tmpPosition] = {"eliminating":tmpGrid,"score":scores.gain}
                        }
                    }

              }

          }



      }

      return possibleMoves
  }

  player1PossibleMoves = (token,backwardAllowed=false) =>{
      let tmpBucket = null
      let possiblePositions = {}

      if(this.isKing(token)){
          return this.getKingPossibleMoves(token.bucket,token.position,token.player)
      }else{

          if(token.bucket>0){
              tmpBucket = token.bucket - 1
          }

          possiblePositions = this.leftRightMovement(token,tmpBucket,1)

          if(backwardAllowed){
              possiblePositions = Object.assign(possiblePositions,this.checkBackMovementCompatibility(token))
          }

      }

      return possiblePositions
  }

  player2PossibleMoves = (token,backwardAllowed=false) =>{
      let tmpBucket = null
      let possiblePositions = {}

      if(this.isKing(token)){
        return this.getKingPossibleMoves(token.bucket,token.position,token.player)
      }else{

          if(token.bucket<7){
            tmpBucket = token.bucket + 1
          }

          possiblePositions = this.leftRightMovement(token,tmpBucket,2)

          if(backwardAllowed){
              possiblePositions = Object.assign(possiblePositions,this.checkBackMovementCompatibility(token))
          }

      }

      return possiblePositions
  }


  tokenCanMove = (token,backwardAllowed=false) =>{

      if(parseInt(token.player)===1){

        return this.player1PossibleMoves(token,backwardAllowed)

      }else{

        return this.player2PossibleMoves(token,backwardAllowed)

      }
  }

  /*
    function to get best move for our AI
  */
  getAllPossiblePlayerMoves = (depth=0) =>{
      this.allowBoardUpdates = false
      const currentPlayer = this.playerTurn
      const currentState = {...this.gameState}
      let movableTokens = {}


      for (let [key] of Object.entries(currentState)) {

          if(currentState[key].player !== parseInt(this.playerTurn)){
            continue
          }else{

                  let tmp = this.tokenCanMove( currentState[key] )
                  if( Object.keys(tmp).length !== 0 ){
                      for(let [key1] of Object.entries(tmp)){

                          let tmpMove = {}
                          tmpMove[key1] = { 
                                            "player":currentPlayer,
                                            "depth":depth,
                                            "eliminating":tmp[key1].eliminating,
                                            //here if the depth is zero, we multiply score by 10 because, we wan to ensure
                                            //that available eliminations are always the refered choice 
                                            "score":depth===0 ? 10 * tmp[key1].score : tmp[key1].score, 
                                            "moves":{}
                                          }

                          if(movableTokens[key]===undefined){
                            movableTokens[key] = tmpMove
                          }else{
                            movableTokens[key] = Object.assign(movableTokens[key],tmpMove)
                          }

                      }

                  }

          }

      }


      for(let [key] of Object.entries(movableTokens)){
          this.possiblePositions = {...movableTokens[key]}
          for(let [key1] of Object.entries(movableTokens[key])){

              //reseting state to how it was when we entered the function
              //as looping will update the game states while in play
              this.gameState = {...currentState}
              this.playerTurn = currentPlayer
              this.possiblePositions = this.tokenCanMove(currentState[key])

              this.activePosition = key
              let position = parseInt(key1.substring(1,2))
              let bucket = parseInt(key1.substring(0,1))
              let isKing = this.isKing(this.gameState[key])

              let targetedForElimination = movableTokens[key][key1].eliminating
              //if eliminating king then increase the score for this move
              if(targetedForElimination!==false){
                if(this.isKing(this.gameState[targetedForElimination])){
                  movableTokens[key][key1].score += scores.becomeKing
                }
              }


              let res = this.gridManipulations(key1,position,bucket)
              

              if(this.playerTurn===currentPlayer){
                movableTokens[key][key1].score += scores.gain
                for(let [repKey] of Object.entries(this.possiblePositions)){
                  this.activePosition = key1
                  position = parseInt(repKey.substring(1,2))
                  bucket = parseInt(repKey.substring(0,1))
                  
                  res = this.gridManipulations(repKey,position,bucket)
                  key1 = repKey
                  break
                }
              }

              if(this.isKing(this.gameState[key1]) && !isKing){
                //fixing weird bug of marking this token as undefined
                if(movableTokens[key][key1]===undefined){
                    continue
                }
                movableTokens[key][key1].score+=scores.becomeKing
              }

              if( res === null && depth !== this.maximumDepth ){
                  const nextStepMoves = this.getAllPossiblePlayerMoves(depth+1)

                  //fixing weird bug of marking this token as undefined
                  if(movableTokens[key][key1]===undefined){
                      continue
                  }
                  movableTokens[key][key1].moves = Object.assign( movableTokens[key][key1].moves, nextStepMoves )
                  
              }

          }

      }



      return movableTokens
  }

  //this function checks to see if the current user has moves left
  //this is to ensure the game never ends up in a locked state where users can not move
  canThePlayerMove = (player,stopOnlyOnElimination = false) =>{

      for (let [key] of Object.entries(this.gameState)) {
        if(this.gameState[key].player!== parseInt(player)){
          continue
        }else{
          const tmp = this.tokenCanMove(this.gameState[key])
          //if length is greater than 0 means one or more of the tokens can move hence game can continue
          //so we return true immediatelly
          if(stopOnlyOnElimination===false){
	          if( Object.keys(tmp).length !== 0 ){
	            return true
	          }          	
          }else{
          	for(let [key1] of Object.entries(tmp)){
          		if(tmp[key1].eliminating!==false){
          			return true
          		}
          	}
          }
        }
      }
      
      return false    
  }

  /*
    MULTIPLAYER MODE FUNCTIONALITIES
  */
  updateOnlineStatus = () =>{
    const now = new Date().getTime()
    if(now - this.lastOpponentOnlineTimestamp > 40 ){
      if(this.opponentOnline === true){
          this.opponentOnline = false
          this.forceUpdate()        
      }
    }else{
      if(this.opponentOnline === false){
          this.opponentOnline = true
          this.forceUpdate()        
      }
    }

    if(!this.againstAI){
        this.secondsCounter++
        if(this.secondsCounter%30===0){
          this.updateMoveToOpponent("online",null,0)
        }      
    }
  }

  //This is the function that sends whatever we need to our opponent, it uses the server as a relay
  updateMoveToOpponent = async (action="move",grid=null,winner=0) =>{
    if(!this.againstAI){
        let movement = null
        if(grid!=null){
          movement = {
            "active_position":this.activePosition,
            "grid":grid,
            "bucket":parseInt(grid.substring(0,1)),
            "position":parseInt(grid.substring(1,2)),
            "possible_positions":this.possiblePositions
          }
        }

        const payload = {
            "opponent_name":this.player2Name,
            "opponent_points":this.player2Points,
            "player":this.me,
            "action_type":action,
            "user_id":this.userId,
            "session":this.receivedSessionId,
            "state":this.gameState,
            "winner":winner,
            "player_turn":this.playerTurn,
            "target_recipient":this.targetRecipient,
            "movement":movement
        }

        try{
          if(this.gameFinnishedLoading){
               let res = await APICall("game-activity",payload,"POST")
               if(res.body.code!==200){
                this.Alert("Tafadhali hakikisha wewe na mpinzani wako mna intaneti")
               }
          }
        }catch(e){
            console.log(e)
        }
 
    }
  }

  copyLinkToclipBoard = () =>{
    this.setDifficulty("human")
    this.boardInitialization()
    this.setUpBoard()


    /* Get the text field */
    var copyText = document.getElementById("linkField");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    this.Alert("Link imekopiwa kikamilifu");
    LogAnalyticsEvent("In Game Activity","Link Copied",null,true)
  }


  initializeGame = () =>{
    if(this.termsAgreed===false){
      return this.Alert("Tafadhali kubaliana na vigezo na masharti kwanza")
    }

    if( this.opponentSelected === false){
      return this.Alert("Tafadhali chagua aina kati ya kucheza na NBC au kucheza na rafiki")
    }

    if(this.me===2){      
      this.updateGameSession()
    }

    /*
      It is at this point that we set the user as having played a game
    */
    const tmpUser = {
      "id":this.userId,
      "games":1,
      "weekly_games":1
    }
    APICall("update-user-stats",tmpUser,"POST");
    /////

    this.seconds = this.minutes = 0
    //if not againsta AI, then clear the timer and stop the times all together and wait for the opponet to join
    if(this.againstAI!==true){
      this.SetUpSocket()
      this.matched = false
      clearInterval(this.timer)
    }else{
      //i am player 1
      this.me = 1
      this.matched = true      
    }

    this.gameInitialized = true
    this.forceUpdate()
    LogAnalyticsEvent("In Game Activity","Game Initialized",null,true)
  }

  termsSwitching = (e) =>{
    this.termsAgreed = !this.termsAgreed
  }

  //this is the function that sets up the game session in the server 
  //it is this game session that will be matced on by the second player
  setUpGameSession = async () =>{
    if(this.userId!==null){
      this.me = 1

     const game = {
       "player_1":this.userId,
     }

     if (this.player1Name=="Player 1"){
        const tmpUser = await this.getUserFromDB(this.userId)
        if(tmpUser!==null){
          this.player1Name = tmpUser.name
          this.player1Points = tmpUser.total_points
        }
     }

     let res = await APICall("create-game-session",game,"POST")
     if(res.body.code===200){
        this.receivedSessionId = res.body.payload.ID
        this.gameLink = domain+res.body.payload.session_id
        this.forceUpdate()
     }else{
      this.Alert("1- Kuna tatizo la kiufundi limejitokeza, tafadhali jaribu tena!")
     }

    }
  }

  getUserFromDB = async(userID) =>{
    let res = await APICall("get-user?userId="+userID,null,"GET")
    if(res.body.code===200){
        return res.body.payload
    }else{
     this.Alert("1a- Kuna tatizo la kiufundi limejitokeza, tafadhali jaribu tena!")
    }

    return null
 }

  updateGameSession = async () =>{
    const gameSession = {
      "id":this.receivedSessionId,
      "player2":this.userId,
      "status":"Matched"
    }

    let res = await APICall("update-game-session",gameSession,"POST")
    if(res.body.code===200){
      this.matched = true
      this.updateMoveToOpponent("matched")
      this.forceUpdate()
      let self = this
      this.timer = setInterval(function(){
        self.addTime()
      },1000)          
    }else{
      this.Alert("2- Kuna tatizo la kiufundi limejitokeza, tafadhali jaribu tena!")      
    }
  }

  Confirm = (errorMessage) => {
    this.errorType = "Confirm"
    this.errorMessage = errorMessage
    const banner = banners[Math.floor(Math.random() * banners.length)];
    this.bannerPath = banner.banner_path
    this.bannerLink = banner.banner_link

    this.forceUpdate()
  }

  Alert = (errorMessage) =>{
    this.errorType = "error"
    this.errorMessage = errorMessage
    const banner = banners[Math.floor(Math.random() * banners.length)];
    this.bannerPath = banner.banner_path
    this.bannerLink = banner.banner_link

    this.forceUpdate()
  }

  getGameSession = async () =>{
    if(this.userId!==null){
     let res = await APICall("get-game-session?sessionId="+this.sessionId,null,"GET")
     if(res.body.code===200){
        if(res.body.payload.status==="Pending"){
            this.opponentSelected = true
            this.againstAI = false
            this.player1Name = res.body.payload.player_1_name
            this.player1Points = res.body.payload.player_1_points === undefined ? 0 : res.body.payload.player_1_points
            this.receivedSessionId = res.body.payload.ID 
            this.targetRecipient = this.receivedSessionId+"-"+res.body.payload.player_1
            //i am player 2
            this.me = 2


            if (this.player2Name=="Player 2"){
              const tmpUser = await this.getUserFromDB(this.userId)
              if(tmpUser!==null){
                this.player2Name = tmpUser.name
                this.player2Points = tmpUser.total_points
              }
           }
      


//            this.player2Name = res.body.payload.player_2_name || "JINA LANGU"
            this.gameLink = "Ingia kucheza na "+this.player1Name          
            if(this.userId===res.body.payload.player_1){
              this.gameLink = "Karibisha mtu mwingine kucheza"         
              this.samePlayerAlert = true 
            }
          }else{
            this.gameLink = "Mchezo huo tayari umepata wachezaji wawili."
            this.matched = true
          }

     }else{
      this.Alert("3- Kuna tatizo la kiufundi limejitokeza, tafadhali jaribu tena!")
     }

    }
  }


  getTop10 = async (targetGroup="weekly") =>{
     this.currentWinnersGroup = targetGroup
     this.top10Loading = true
     this.forceUpdate()
     let res = await APICall("get-rankings?start=0&count=10&rankingType="+targetGroup,null,"GET")
     this.top10Loading = false
     if(res.body.code===200){
        this.currentTop10 = res.body.payload
     }else{
      this.Alert("4- Kuna tatizo la kuifundi limetokea, tafadhani laribu tena baadaye")
      console.log(res)
     }
     this.forceUpdate()
  }


  responseFacebook = (e) =>{
    try{
      if(e.name!==undefined){
          const user = {
            "name":e.name,
            "login_key":e.userID,
            "password":e.userID,
            "channel":"facebook"
          }

          this.LoginUser(user)
          LogAnalyticsEvent("In Game Activity","User logged in","facebook",true)

      }else{
        this.Alert("Tumeshindwa kukutambua kupitia facebook!")
      }      
    }catch(e){
      console.log(e)
    }

  }


  googleResponse = (e) =>{
    try{

      if(e.profileObj!==undefined){
          const user = {
            "name":e.profileObj.name,
            "login_key":e.profileObj.googleId,
            "password":e.profileObj.googleId+"-google",
            "channel":"google"
          }

          this.LoginUser(user)
          LogAnalyticsEvent("In Game Activity","User logged in","google",true)

      }else{
        this.Alert("Tumeshindwa kukutambua kupitia google!")
      }      

    }catch(e){
      console.log(e)
    }
  }

  LoginUser = async (userDetails) =>{
      let name = ""
      let res = await APICall("admin-login",userDetails,"POST")
      if(res.body.code===200){
            this.userId = res.body.payload.ID
            this.userName = name = res.body.payload.name

            setSessionStorage("clientId",this.userId)
            setSessionStorage("clientName",this.userName)
            // setCookie("clientId",this.userId)
            // setCookie("clientName",this.userName)
      }else{
        res = await APICall("add-user",userDetails,"POST")
        if(res.body.code===200){
            this.userId = res.body.payload.ID
            this.userName = name = res.body.payload.name

            setSessionStorage("clientId",this.userId)
            setSessionStorage("clientName",this.userName)

        }else{
          this.Alert("5- Kuna tatizo limetokea, tafadhali jaribu tena")
          return
        }
      }


      if(this.sessionId===""){
        this.setUpGameSession()
        this.player1Name = name
        this.player1Points = res.body.payload.points === undefined ? 0 : res.body.payload.points
        this.me = 1
      }else{
        this.getGameSession()
        this.player2Name = name
        this.player2Points = res.body.payload.points === undefined  ? 0 : res.body.payload.points
        this.me = 2
      }

  }

  showOppositionOptions = (option) =>{
    document.getElementById("withNBC").style.display="none"
    document.getElementById("withAFriend").style.display="none"
    switch(option){
      case "friend":
      document.getElementById("withAFriend").style.display="block"
      this.setDifficulty("Human")
      break
      case "nbc":
      this.setDifficulty("hard")
//      document.getElementById("withNBC").style.display="block"
      break
      default:
      return
    }
  }

  scrollToWinners = () =>{
//    document.getElementById("winnersZoneToScrollTo").scrollIntoView()
    const targetElement = document.getElementById("winnersZoneToScrollTo")
    document.getElementById("initialize").scrollTo({
        top: targetElement.offsetTop,
        behavior: 'smooth'
    });
  }

  scrollToLogin = () =>{
//    document.getElementById("winnersZoneToScrollTo").scrollIntoView()
    const targetElement = document.getElementById("loginSection")
    document.getElementById("initialize").scrollTo({
        top: targetElement.offsetTop,
        behavior: 'smooth'
    });
  }


  render(){

      let top3 = <div className="col-12 text-center">
                    <p></p>
                    <h2>Kwasasa hamna mshindi</h2>
                  </div>
      //
      if(this.top10Loading){
          top3 = <div className="col-12 text-center">
                    <h2><span className="fa fa-spin fa-spinner"></span> matokeo yanaandaliwa</h2>
                  </div>
       }

      //
      if(this.currentTop10.length>0 && this.top10Loading===false){

        //if first user in the list has no points we drop the whole list
        if(
            (this.currentWinnersGroup==="weekly" && this.currentTop10[0].weekly_points===0) ||
            (this.currentWinnersGroup!=="weekly" && this.currentTop10[0].total_points===0)
          ){
          top3 = <div className="col-12 text-center">
                    <p></p>
                    <h2>Kwasasa hamna mshindi</h2>
                  </div>
          //
        }else{

          top3 = this.currentTop10.map((row,index)=>{
            if(index>2){
              return null
            }
            let noHere = "kwanza"
            if(index===1){
              noHere = "pili"
            }else if(index===2){
              noHere = "tatu"
            }

            //if zero points remove user from list
          if(
              (this.currentWinnersGroup==="weekly" && row.weekly_points===0) ||
              (this.currentWinnersGroup!=="weekly" && row.total_points===0)
            ){
            return null
          }

            return    <div className="col-sm-12 col-md-6 winner" key={index}>
                                <img src="./assets/images/trophy.png" className="img-fluid winnerIcon" alt="Winner" />
                                <div className="winnerHolder">
                                  <span className="winnerName">
                                      {index+1}. {row.name}
                                  </span><br/>
                                  <span className="winnerPoints">
                                      Pointi: {this.currentWinnersGroup==="weekly" ? numberWithCommas(row.weekly_points) : numberWithCommas(row.total_points)}
                                  </span><br/>
                                  <span className="winnerPoints">
                                      Michezo: {this.currentWinnersGroup==="weekly" ? numberWithCommas(row.weekly_games) : numberWithCommas(row.games)}
                                  </span>
                                </div>
                            </div>

          })

        }


      }


      this.canvas = this.board.map((row,index)=>{
        return (
          <div key={index} className="segment" id={index}>
            {this.fillSegment(row,index)}
          </div>
          )
      })

      //
	  	return(
        <>
          <Menu 
            UserId={this.userId}
            UserName={this.userName}
            Logout={this.logoutUser}
            GoToLogin={this.scrollToLogin}
          />
          {
            this.errorType !== null ? 
              <div id="errorTakeOver">
                <div className="container">
                      <div className="row">
                        {
                          this.bannerLink !== null ? 
   
                            <div className="col-10 m-auto text-center" id="bannerZone">
                              <a href={this.bannerLink} rel="noreferrer noopener" target="_blank">
                                    <img src={this.bannerPath} className="img-fluid" />
                              </a>
                            </div>

                          : null
                        }
                        <div className="col-10 m-auto text-center" id="errorZone">{this.errorMessage}</div>
                        <div className="col-12 m-auto text-center">
                          <p></p>
                          {
                            this.errorType === "error" ? 
                            <button className="btn btn-lg btn-danger" onClick={()=>this.clearError()}>SAWA</button> :
                            <>
                                <button className="btn btn-success" data-dismiss="modal" onClick={()=>this.clearError()} >Hapana</button>
                                <span>&nbsp;</span>
                                <button className="btn btn-primary"  onClick={()=>this.yesRestart()}>Ndio</button>

                            </>
                          }
                        </div>
                      </div>
                </div>
              </div>

            : null
          }

          {
            this.gameInitialized === false ?
            <div id="initialize">
              <div className="row" id="checkerdBg">
                <div className="container">
                    <div className="row">
                      <div className="col-12">
                        <div className="row">
                          <div className="col-sm-12 col-md-6">
                          </div>
                          <div className="col-sm-10 col-md-6" id="bofyaSection">
                            <h1 className="mastheadTitle">
                              ONESHA UWEZO WAKO WA KUCHEZA DRAFTI LA NBC.<br/>
                              CHEZA NA NBC AU MUALIKE MWENZAKO
                            </h1>
                            <button className="bofyaHapaButton" onClick={()=>this.scrollToWinners()}>BOFYA HAPA KUCHEZA</button>
                          </div>
                        </div>
                      </div>


                    </div>
                </div>
              </div>

              <hr/>

              <div className="row ">
                <div className="container" id="winnersZoneToScrollTo">

                      <div className="col-12 m-auto whiteText WinnersSection">
                        <p></p>
                        <div className="text-center">
                          <h1>Wakali wa drafti la NBC</h1>
                          <button 
                            className={"btn winnersSelectionBtn "+(this.currentWinnersGroup==="weekly" ? "active": "")}
                            onClick={()=>this.getTop10("weekly")}
                            >
                            Washindi wa wiki hii
                          </button>
                          &nbsp;&nbsp;
                          <button 
                            className={"btn winnersSelectionBtn "+(this.currentWinnersGroup!=="weekly" ? "active": "")}
                            onClick={()=>this.getTop10("all")}
                            >
                            Washindi wa ujumla
                          </button>
                        </div>
                        <div className="row">
                          <div className="col-12">
                            <p></p>
                            <h4 className="text-center">Washindi wa {this.currentWinnersGroup==="weekly"? "wiki" :"ujumla"} </h4>
                          </div>
                          {top3}
                        </div>
                      </div>

                      {
                         this.userId !== null ?

                      <div className="col-12 m-auto whiteText WinnersSection">
                        <div className="row">
                          
                          {
                            this.sessionId === "" || this.sessionId === undefined ? 
                            <>
                                  <div className="col-12 m-auto oppositionOptionEnclose">
                                    <p></p>
                                    <h1>Chagua namna ungependa kucheza</h1>
                                    <p></p>
                                      <h1>
                                        <label>
                                          <input name="oppositionOption" type="radio" value="nbc" onClick={()=>this.showOppositionOptions("nbc")} /> 
                                          <span>&nbsp;</span>
                                          Jifunze na NBC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      </label>
                                      </h1>
                                      <div className="opponentOption text-center" id="withNBC">
                                        <p>
                                          <label className="difficultySelection cursor">
                                            <input type="radio" className="levelSelector" name="dificulty" value={1} onClick={()=>this.setDifficulty("easy")} />&nbsp;Rahisi&nbsp;&nbsp;&nbsp;
                                          </label>
                                        </p>
                                        <p>
                                          <label className="difficultySelection cursor">
                                            <input type="radio" className="levelSelector" name="dificulty" value={3} onClick={()=>this.setDifficulty("medium")} />&nbsp;Wastani
                                          </label>
                                        </p>
                                        <p>
                                          <label className="difficultySelection cursor">
                                            <input type="radio" className="levelSelector" name="dificulty" value={5} onClick={()=>this.setDifficulty("hard")} />&nbsp;Ngumu&nbsp;
                                          </label>
                                        </p>
                                      </div>

                                      <p></p>
                                      <h1>
                                        <label>
                                          <input type="radio" name="oppositionOption" value="friend" onClick={()=>this.showOppositionOptions("friend")} /> 
                                          <span>&nbsp;</span>
                                          Cheza na rafiki yako
                                        </label>
                                      </h1>
                                      <div className="opponentOption" id="withAFriend" >
                                        <h4>
                                          Nakili ‘link’ hapa chini kisha tuma kwa mtu unayetaka kucheza nae.
                                        </h4>
                                          <div className="w-100 copyLinkRegion text-center" onClick={this.copyLinkToclipBoard}>
                                            {this.gameLink} 
                                            <span>&nbsp;&nbsp;</span>
                                            <span className="fa fa-copy"></span>
                                          </div>

                                          {                
                                            this.isMobile.any()!==null && this.isMobile.any()!== undefined  ?
                                            <a href={"https://api.whatsapp.com/send?text=Karibu tucheze gemu ya drafti kutoka NBC \r\n"+this.gameLink} target="_blank" rel="nofollow noreferrer noopener" className="btn btn-success w-100 whatsappInvite">
                                                <span className="fab fa-whatsapp"></span> Tuma kwa WhatsApp
                                            </a> 
                                            :
                                            null
                                          }


                                          <input type="text" 
                                                 className="w-100 text-center cursor noVissible" 
                                                 id="linkField" 
                                                 readOnly={true} 
                                                 value={this.gameLink} 
                                                 onClick={this.copyLinkToclipBoard}
                                          />
                                      </div>


                                  </div>
                            </>
                              :
                              <div className="col-12">
                                <p></p>
                                <h1>{this.gameLink}</h1>
                                <hr/>
                              </div>
                          }


                          {
                            this.matched === true ? 
                            <div className="col-sm-12">
                                <a href="/" className=" cursor" id="startGameButton" onClick={this.initializeGame}>
                                  Anza gemu nyingine
                                </a>
                            </div>
                          :

                            this.samePlayerAlert === false ? 
                              <div className="clearfix col-12">
                                    <p className="">
                                      <label id="termsAndConditionsAgree">
                                        <input type="checkbox" 
                                               value={false} 
                                               onClick={this.termsSwitching} 
                                               checked={this.termsAgreed}
                                               readOnly={true}
                                               /> Nakubaliana na kanuni na masharti ya kushiriki.
                                      </label>
                                    </p>
                                  <button className="text-center cursor w-100" id="startGameButton" onClick={this.initializeGame}>
                                    CHEZA
                                  </button>
                              </div>
                            : null
                          }
                        </div>
                      </div>


                          :
                          <div className="col-12 text-center whitetext_" id="loginSection">
                            <div className="hidden" id="fbHolder">
                              <FacebookLogin
                                appId="664536710797742"
                                autoLoad={false}
                                fields="name,email,picture"
                                callback={this.responseFacebook} />
                            </div>

                            <div className="hidden" id="googleHolder">
                              <GoogleLogin
                                  clientId="1066258160351-5id6d90efc4m5gadm8mc14c87bvpvkeq.apps.googleusercontent.com"
                                  buttonText="Login"
                                  onSuccess={this.googleResponse}
                                  cookiePolicy={'single_host_origin'}
                                  className="kep-login-google"
                                />
                            </div>


                            <p></p>
                            <h1 className="whiteText_">Ingia kwa kutumia akaunti unayopendelea</h1><br/>
                            <button className="btn loginBtn facebookButton whiteText" onClick={()=>{document.getElementsByClassName("kep-login-facebook")[0].click()}}>
                              <span className="fab fa-facebook"></span> Ingia
                            </button>

                            <button className="btn loginBtn GmailButton whiteText" onClick={()=>{document.getElementsByClassName("kep-login-google")[0].click()}}>
                              <span className="fab fa-google"></span> Ingia
                            </button>

                          </div>
                      }
                      
                      <div className="col-12 m-auto whiteText_">
                          <p></p>
                          <h1>Kanuni na Masharti ya Kucheza Drafti la NBC</h1>
                          <div className="text-justified">
                            <ol type="1">
                              <li>Ili kuweza kucheza, utatakiwa kufungua akaunti kwa kutumia akaunti yako ya Facebook au Gmail;</li>
                              <li>Ili uweze kufungua akaunti ni lazima uwe na umri wa miaka 18 au zaidi;</li>
                              <li>Upo katika eneo ambalo ni halali kufungua akaunti nasi na kutumia huduma zetu;</li>
                              <li>Wewe ni mtu kama unavyotamka katika usajili wako, na unatoa taarifa sahihi na kamilifu;</li>
                              <li>Unajisajili kama mhusika mkuu na si kwa niaba ya mtu mwingine;</li>
                              <li>Hujawahi kuwa na akaunti ambayo imepata kufungiwa kwa sababu za ukiukaji au matumizi mabaya au tabia iliyo kinyume cha sheria;</li>
                              <li>Kumbuka, unaweza kucheza na bot la NBC au na mtu mwingine</li>
                            </ol>


                          </div>
                          <hr/>
                          <span className="btn blueBtn" data-toggle="modal" data-target="#myModal">Soma sera yetu ya Usiri na Faragha</span>
                          <span>&nbsp;&nbsp;</span>
                          <span className="btn blueBtn" data-toggle="modal" data-target="#myHowtoplay">Jinsi ya Kucheza</span>
                          <p></p>
                      </div>

                </div>
              </div>

            </div>
            :
            null
          }

          {
            this.OpenWinnersCover === true ?
              <div id="cover">
                <div className="container">
                  <div className="row">
                    <div className="col-sm-12 col-md-8 m-auto text-center whiteText" id="coverContent">
                          <h2 className="clearfix">
                              <span className="fa fa-home float-left cursor coverItem" onClick={()=>this.restartGame(false)}></span>
                              <span className="fa fa-times float-right cursor coverItem" onClick={this.closeWinnersCover}></span>
                          </h2>
                          {this.gameEndTitle}
                          {this.gameEndStatement}
                          <h2 className="clearfix">
                            <p><br/></p>
                            <span className="float-left_ cursor coverItem" onClick={()=>this.restartGame(false)} >
                                <span className="fa fa-play-circle fa-2x"></span><br/>
                                <span>CHEZA<br/>TENA</span>
                            </span>

                            <span className="float-right hidden">
                                <span className="fa fa-star fa-2x yellowText"></span><br/>
                                <span>KUSANYA<br/>POINTI</span>
                            </span>

                          </h2>
                    </div>
                  </div>
                </div>
              </div>
            :
            null
          }



    			<div className="container">
            <div className="text-center">
              <a target="_blank" rel="noreferrer nofollow" 
                  href="https://www.nbc.co.tz/sw/personal/insurance/explore/?utm_source=nbc_checkers">
                  <img src="./assets/images/slide.png" alt="slide" className="img-fluid" />
              </a>
            </div>
            <div className="gameSegment">
                
                <div className="controls sidePanels whiteText">

                  <span className="fa fa-home control cursor" onClick={()=>this.restartGame(false)}></span>
                  {
                    this.againstAI===true?
                    <span className="fa fa-undo control cursor text-danger hidden" onClick={this.goBack}></span> :
                    <span className="fa fa-undo control cursor text-danger hidden" onClick={()=>this.restartGame(false)}></span> 
                  }

                  {
                    this.historyIteration > 0 ?
                    <span className="fa fa-play text-success control cursor" onClick={this.continueWithGame}></span>
                    :null
                  }
                  <span className={"fa control cursor "+(this.allowSounds===true ?"fa-volume-up":"fa-volume-mute")} onClick={this.toggleSounds}></span>

                </div>
                
                <div className={"board "+(this.me===2 ? "rotated" : "")}>
        	  			{this.canvas}              
                </div>

                  {
                    this.me===1 ?
                    <div className="score sidePanels">
                      <div className="opponentRepresentation player1">
                        <span>{this.captures.player2_captures}</span>
                      </div>
                      <div className="opponentRepresentation player2">
                        <span>{this.captures.player1_captures}</span>
                      </div>
                    </div> 
                    :
                    <div className="score sidePanels">
                      <div className="opponentRepresentation player2">
                        <span>{this.captures.player1_captures}</span>
                      </div>
                      <div className="opponentRepresentation player1">
                        <span>{this.captures.player2_captures}</span>
                      </div>
                    </div>                   
                  }
            </div>

            <div className="timer whiteText">
              <strong>MUDA : </strong>
              <span>
                  { 
                    this.minutes<10 ? 
                    "0"+this.minutes:
                    this.minutes
                  } 
                  &nbsp;:&nbsp;
                  {
                    this.seconds<10 ?
                    "0"+this.seconds:
                    this.seconds
                  }
              </span>
              {
                this.againstAI===true ? 
                <span className={"playPause cursor fa "+(this.gamePaused===false ? "fa-pause" :"fa-play")} onClick={this.pausePlay}></span>
                : null
              }
            </div>

            <div className="playersSegment">


                <div className="player">
                    <div className={"turnSignal "+(this.playerTurn===1 ? "myTurn" : "")}>
                      <img src="./assets/images/player-icon.png" alt="player1" className="img-fluid playerIcon player1Icon" />
                    </div>
                    <span>{this.player1Name}</span>
                    <span className="points">Point : {this.player1Points>10 ? this.player1Points : "0"+this.player1Points}</span>
                </div>

                {
                  this.matched===true ? 
                    <div className="player">
                        <div className={"turnSignal "+(this.playerTurn===2 ? "myTurn" : "")}>
                          <img src="./assets/images/player-icon.png" alt="player1" className="img-fluid playerIcon player2Icon" />
                        </div>
                        <span>{this.againstAI === true ? "NBC" : this.player2Name}</span>
                        {
                          this.againstAI===true ? <span>&nbsp;</span> : 
                          <span className="points">Point : {this.player2Points > 10 ? this.player2Points : "0"+this.player2Points}</span>
                        }
                    </div> :
                    <div className="player">
                      <h5>Tunamsubiria mpinzani <br/>wako ajiunge</h5>
                    </div>
                }
                <div className="hidden">
                  <img src="./assets/images/P1K.png" alt="preloader"/>
                  <img src="./assets/images/P2K.png" alt="preloader"/>
                </div>
            </div>
  	  		</div>
          <PrivacyPolicy />
          <HowToPlay />
        </>
  		)
  }
}

//
function PrivacyPolicy(){
  return(
<div className="modal" id="myModal">
  <div className="modal-dialog modal-md">
    <div className="modal-content">

      <div className="modal-header">
        <h4 className="modal-title">Sera yetu ya Usiri na Faragha</h4>
        <button type="button" className="close" data-dismiss="modal">&times;</button>
      </div>

      <div className="modal-body">
        <p>
          Benki ya Taifa ya Biashara inazingatia utunzaji siri wa taarifa za wateja wetu. 
          Tunapozungumzia ‘taarifa za wateja wetu’ tunamaanisha taarifa zozote kuhusu mteja ambazo 
          mteja mwenyewe au mtu mwingine ameziwasilisha kwetu.
        </p>
        
        <p>
          Uamuzi wa mteja kujipatia NBC taarifa zake kwa njia ya mtandao huwa ni chaguo la mteja 
          mwenyewe. Hata hivyo, mteja anapaswa kujua kwamba miamala yoyote ya kifedha zinazozuia 
          Baadhi ya taarifa inaweza kuipa NBC mipaka katika huduma tunazozitoa.
        </p>

        <p>
        <strong>Saidia katika kulinda siri za mteja</strong><br/>
        Mteja pia anaweza kusaidia katika kudumisha usiri wa taarifa za kibenki kwa
        </p>
        <ol type="i">
          <li>
            Kutomshirikisha mwingine kuhusiana na taarifa za Kitambulisho cha Mteja 
            au namba yake ya siri
          </li>
          <li>Kubadili namba ya siri ya mteja mara kwa mara</li>
          <li>Kufunga akaunti kila mara anapofanya miamala ya kibenki kwa njia ya mtandao</li>
          <li>Kutoacha kompyuta ya mteja bila mtu wakati wa kutekeleza miamala ya mtandaoni</li>
          <li>Kutoa taarifa ya upotevu au kuibiwa kwa kadi</li>
          <li>Kuisaidia benki ya NBC kuwa na kumbukumbu sahihi kwa kutoa taarifa kunapotokea 
          mabadiliko yoyote kwa upande wa mteja ili waweza pia kusasaisha kumbukumbu zake</li>
          <li>Kutotuma taarifa yoyote ya siri kupitia b-pepe isiyo na namba ya siri</li>
          <li>Kutotoa taarifa yoyote ya binafsi na ya kifedha kwenye mazingira yoyote au 
          kwa mtu yeyeyote ambaye humwamini</li>
          <li>Kutofanya miamala wala kuwezesha miamala ya NBC kwa kutumia chombo kisicho 
          na usalama (simu ya mkononi, mtandao, nk.)</li>
        </ol>

        <p>
        <strong>Kulinda taarifa za mteja</strong><br/>
        </p>
        <ol type="i">
          <li>
              <strong>Taarifa za mteja ni nini?</strong><br/>
              Taarifa za mteja ni taarifa binafsi na za siri zinazomtambulisha au zinazohusiana naye 
              ambazo hupelekwa NBC kupitia tovuti, mteja anaweza kuwa na mtu binafsi au biashara. 
              Taarifa hizi zinahusisha jina, umri, namba za Kitambulisho, namba za usajili, anuani na 
              awasiliano mengine, utambulisho wa kijamii, jinsia, dini, madeni, taarifa za kipato na 
              malipo, taarifa za fedha na taarifa za kibenki kama vile namba ya akaunti.
          </li>
          <li>
            <strong>Kwa nini benki ya NBC hukusanya taarifa za mteja?</strong><br/>
            Benki ya NBC hutumia taarifa za mteja ili kumtambua na kumpatia huduma bora za mtandaoni. 
            Taarifa za mteja ni muhimu pia katika kuisaidia NBC kuwasiliana na mteja tunapokuwa na 
            maswali yoyote. NBC pia hutumia taarifa za mteja ili kumfahamisha mteja juu ya huduma na 
            bidhaa nyingine zinazotolewa na NBC. Hivyo, kadiri tunavyokufahamu vizuri, ndivyo 
            tunavyoweza kukuhudumia vizuri pia.
          </li>
          <li>
            <strong>Benki ya NBC inalindaje usalama wa taarifa zako?</strong><br/>
            Kutunza kwa usalama taarifa zako za kifedha ni moja ya majukumu makuu ya NBC. 
            Sera hii inaongoza hata mwenendo wa wafanyakazi katika masuala mazima ya usahihi, 
            usiri na usalama wa taarifa zote za mteja.            
          </li>
        </ol>

        <p>
          <strong>Haki ya kurekebisha sera ya usiri</strong><br/>
          NBC ina haki ya kurekebisha sera hii wakati wowote. Marekebisho yote kwenye sera hii 
          yatawekwa kwenye tovuti. Isipokuwa kama itaelezwa vinginevyo, toleo hili la sasa hivi 
          linafuta matoleo mengine yote yaliyotangulia ya sera hii.        
        </p>

        <p>
          <strong>Mawasiliano ya Beruapepe</strong><br/>
          NBC inaweza kutumia taariza za mteja kumtumia mteja huyo taarifa za huduma au bidhaa 
          mpya ambazo anaweza kuzihitaji na wakati fulani kumtumia mteja ujumbe kwa njia ya posta, 
          baruapepe au ujumbe kwa njia ya simu yake ya mkononi ili kumjulisha kuhusu bidhaa na 
          huduma za NBC.        
        </p>

        <p>
          Mteja atakuwa na uchaguzi wa kueleza kwamba hahitaji kupokea ujumbe kama huo kwa siku 
          za baadae, mara tu anapoona ujumbe huo. Hata hivyo, mteja ataendelea kupokea ujumbe 
          kuhusu bidhaa nyingine mpaka hapo atakapochagua kutopokea taarifa za bidhaa hiyo tena.
        </p>

        <p>
          Kumbuka kuwa upokeaji wa baruapepe zisizo na usiri si salama. Kwa mantiki hiyo, 
          NBC haitaweka taarifa za siri za akaunti yako kwenye majibu ya baruapepe.
        </p>

        <p>
          Aidha, NBC haitamuomba mteja kutoa taarifa binafsi kwa njia ya baruapepe, mfano namba ya 
          akaunti, Namba ya Utambulisho BInafsi wala namba ya siri.
        </p>

        <p>
          <strong>Maadili ya Kibenki</strong><br/>
          Benki ya NBC inaunga mkono na kusisitiza utekelezaji wa Maadili ya Kibenki.
        </p>
      </div>

      <div className="modal-footer">
        <button type="button" className="btn btn-danger" data-dismiss="modal">Funga</button>
      </div>

    </div>
  </div>
</div>
    )
}

//
function HowToPlay(){
  return(
<div className="modal" id="myHowtoplay">
  <div className="modal-dialog modal-md">
    <div className="modal-content">

      <div className="modal-header">
        <h4 className="modal-title">Jinsi ya Kucheza</h4>
        <button type="button" className="close" data-dismiss="modal">&times;</button>
      </div>

      <div className="modal-body">
          <ol type="1">
                <li>Soma kanuni na masharti.</li>
                <li>Jiunge kwa kutumia akaunti ya facebook au Gmail</li>
                <li>
                    Chagua namna ungependa kucheza kati ya<br/>
                    <ol type="a">
                      <li>Kucheza na NBC Bank</li>
                      <li>Kucheza na rafiki yako</li>
                    </ol>
                </li>
                <li>
                    Kucheza na NBC Bank chagua aina ya mchezo kati ya:<br/>
                    <ol type="a">
                          <li>Rahisi</li>
                          <li>Wastani</li>
                          <li>Ngumu</li>
                    </ol>
                </li>
                <li>Kucheza na rafiki yako nakili ‘link’ utakayopewa na kisha tuma ujumbe kwa mtu unayetaka kucheza nae.</li>
                <li>Kubaliana na kanuni na masharti kisha bonyeza  "CHEZA"</li>
                <li>Kuanza kucheza, sogeza kete zako kama drafti la kawaida</li>
                <li>Baada ya kushinda, mshindi atepewa pointi na unaweza kuanza kucheza tena</li>
          </ol>
      </div>

      <div className="modal-footer">
        <button type="button" className="btn btn-danger" data-dismiss="modal">Funga</button>
      </div>

    </div>
  </div>
</div>
    )
}

export default Board;
