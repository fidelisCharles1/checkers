import React from 'react';

export function Menu(props){
  return (
              <>
                <div className="row noMargin fixed-top" id="menuPanel">
                  <div className="col-12">
                    <div className="container noMargin">
                      <div className="row">
                        <div className="col-11 m-auto">

                            <nav className="navbar navbar-expand navbar-light row noPadding">
                              <a className="navbar-brand m-auto" href="#">
                                <img src="https://draft.nbcgames.co.tz/assets/images/logo.PNG" className="img-fluid" id="logo" alt="NBC Logo"/>
                              </a>


                              <button className="navbar-toggler h-100" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                              </button>

                              <div className="collapse navbar-collapse" id="navbarSupportedContent">

                                <div className="ml-auto loginRegister">
                                    <div className="row">
                                      <div className="col-2 centeredElements">
                                        <span className="fa fa-user-circle fa-2x"></span>
                                      </div>
                                      <div className="col">
                                          {
                                            props.UserName !== "" && props.UserName !== null && props.UserName !== undefined ? 
                                            <div className="centeredElements">
                                                <span>{props.UserName || "Fidelis"}</span>
                                                <span onClick={()=>props.Logout()}>Logout</span>
                                            </div> :
                                            <div className="centeredElements">
                                              <strong onClick={()=>props.GoToLogin()} className="cursor">Ingia</strong>
                                            </div>
                                          }
                                      </div>
                                      <div className="col-2 centeredElements hidden">
                                        <span className="fa fa-bars fa-2x cursor" title="Historia" data-toggle="modal" data-target="#myHistory"></span>
                                      </div>
                                    </div>
                                </div>


                              </div>
                            </nav>


                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="buffer"></div>
                <History />
              </>

    )
}

//
function History(){
  return(
<div className="modal" id="myHistory">
  <div className="modal-dialog modal-md">
    <div className="modal-content">

      <div className="modal-header">
        <h4 className="modal-title">Historia</h4>
        <button type="button" className="close" data-dismiss="modal">&times;</button>
      </div>

      <div className="modal-body">
      </div>

      <div className="modal-footer">
        <button type="button" className="btn btn-danger" data-dismiss="modal">Funga</button>
      </div>

    </div>
  </div>
</div>
    )
}
