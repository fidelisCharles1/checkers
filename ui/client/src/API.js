import axios from 'axios';
import ReactGA from 'react-ga';

const eventTimeouts = 15000
//const BaseURL = "http://localhost:8080/" //https://nbcgames.co.tz:8081/"
const BaseURL = "https://draft.nbcgames.co.tz:8081/"

const AnalyticsId = 'UA-177638777-1' 

export function Analytics(){
  ReactGA.initialize(AnalyticsId);
  ReactGA.pageview(window.location.pathname + window.location.search);
}

export function LogAnalyticsEvent(eventCategory,eventAction,eventLabel=null,interactive=true){
  try{
    //this is to workaround an error for when this value is filled
    if(eventLabel!==null){
      eventLabel = ""+eventLabel
    }
    ReactGA.initialize(AnalyticsId);
    ReactGA.event({
      category: eventCategory,
      action: eventAction,
      label: eventLabel,
      nonInteraction: interactive
    });    
  }catch(e){
    console.log(e)
  }
}

export function detectMobileDevice(){
  var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    return isMobile
}


export function logout(){
  setSessionStorage("clientId",null)
  setSessionStorage("clientName",null) 
  setSessionStorage("clientChannel",null)
}

export function getCookie(cname){
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

export function setSessionStorage(name,value){
  window.sessionStorage.setItem(name,value)
}

export function getSessionStorage(name){
  return window.sessionStorage.getItem(name)
}

export function setCookie(cname,cvalue){
//  cvalue = cryptr.encrypt(cvalue);
  document.cookie = cname+"="+cvalue+";expires=Thu, 31 Dec 2030 11:59:00 UTC;path=/"
}

export function numberWithCommas(number){
  number = parseFloat(number).toFixed(2)
  const tmp = number.split(".00")
  if(tmp.length===2){
    number = tmp[0]
  }

  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


export async function APICall(url,payload,method="POST",timeoutAllowed = eventTimeouts){
  let response = {body:null,error:null}
   method = method.toUpperCase()
   if(payload!=null){
        payload = JSON.stringify(payload)
   }
   url = BaseURL+url
   try{
       response.body = await axios({
            method: method,
            url: url,
            data: payload,
            headers: {  
                'Accept': 'application/json',
                'Authorization':  'Basic N0@U3ONg0@)@)',
            },
            timeout: timeoutAllowed,
            withCredentials: true
       })    
       response.body = response.body.data
   }catch(error){
      response.error = error.toString()
   }

  return response
}

