module checkers

go 1.19

require (
	github.com/allegro/bigcache v1.2.1
	github.com/aws/aws-sdk-go v1.44.155
	github.com/dustin/go-humanize v1.0.0
	github.com/gorilla/websocket v1.5.0
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/net v0.4.0
)

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
)
